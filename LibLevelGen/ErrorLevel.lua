local libLevelGen = require "LibLevelGen.LibLevelGen"

local errorLevel = {}

--- Generates a simple placeholder level.
--- @param parameters LibLevelGen.LevelGenerationEventParameters # The parameters from the level generation event
function errorLevel.generate(parameters)
    local instance = libLevelGen.new(parameters)
    instance.levelGeneratorData = {
        allowLevel1Shrine = false,
        allowOldShrines = false,
        placeSecretShops = false,
        placeShrines = false,
        levelsPerZone = 4,
        maxLevel = 20
    }

    local mainSegment = instance:createSegment()
    mainSegment:createStartingRoom()

    instance:finalize()
end

return errorLevel
