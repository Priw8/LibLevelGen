local playableCharacters = require "necro.game.data.player.PlayableCharacters"
local proceduralRun = require "necro.game.data.level.ProceduralRun"
local ecs = require "system.game.Entities"
local levelUtils = require "necro.game.level.LevelUtils"

local enemy = require "LibLevelGen.Enemy"

local entity = {}

-- TODO: enum instead?
entity.Prices = {
    ChestBlack = 40,
    ChestPurple = 30,
    ChestRed = 20,
}

entity.registerPrice = function (type, cost)
    entity.Prices[type] = cost
end

local function findAttribute(attributes, component, field)    if not attributes then
        return nil
    end

    for _, attribute in ipairs(attributes) do
        if attribute.component == component and attribute.field == field then
            return attribute
        end
    end

    return nil
end

--- @param instance LibLegelGen.Instance
local function challangeCharacterActive(instance)
    local charMask = proceduralRun.getPlayerCharacterBitmask(instance.levelGenerationOptions.initialCharacters)
    return bit.band(charMask, bit.bor(playableCharacters.ID.ARIA, playableCharacters.ID.BOLT,
        playableCharacters.ID.MONK, playableCharacters.ID.CODA)) ~= 0
end

local componentForCurrency = {
    coins = "priceTagCostCurrency",
    blood = "priceTagCostHealth"
}

local specialCost = {
    ChestRed = 20,
    ChestPurple = 30,
    ChestBlack = 40
}

local entityMt = {
    --- @class LibLevelGen.Entity
    --- @field instance LibLegelGen.Instance # Instance this entity belongs to
    --- @field type string # Entity type
    --- @field x number # Entity X coordinate relative, relative to the room it's in
    --- @field y number # Entity Y coordinate relative, relative to the room it's in
    --- @field level number # Entity level
    --- @field saleCurrency string | nil # If entity has a price, this determines the currency
    --- @field price table | nil # Entity price data, in the format set by Synchrony's `levelUtils.setEntityPriceTag`
    --- @field attributes table | nil # Additional entity attributes. Use `.setAttribute` and `.getOrCreateAttribute` methods to manipulate this table.
    --- @field inventory table | nil # Entity's inventory. Use the `.giveItem` method to manipulate this table.
    --- Represents a LibLevelGen entity object
    __index = {
        --- !!! Internal
        --- Initialize the entity
        --- @param self LibLevelGen.Entity # The entity
        --- @param instance LibLegelGen.Instance # LibLevelGen instance this entity belongs to
        --- @param entityType string # Entity type
        --- @param x? number # X coordinate of the entity, relative to the room it's in. Defaults to -1
        --- @param y? number # Y coordinate of the entity, relative to the room it's in Defaults to -1
        --- @param level? number | number[] # Level of the entity, if a table is specified it's picked randomly from the provided values. Defaults to 1.
        init = function (self, instance, entityType, x, y, level)
            if type(entityType) ~= "string" then
                log.error("Entity.init: type is not a string, falling back to ResourceHoardGold")
                entityType = "ResourceHoardGold"
            end
            if type(level) == "table" then
                level = instance:randChoice(level)
            end
            self.instance = instance
            self.type = entityType
            self.x = x or -1
            self.y = y or -1
            self.level = level or 1
            if not instance.levelGeneratorData.disableEnemyUpgrades then
                self:upgrade()
            end
        end,

        --- !!! Internal
        --- @param self LibLevelGen.Entity
        --- @param upgradeData table<number,number|number[]>
        processUpgrade = function(self, upgradeData)
            if upgradeData then
                local upgrade = upgradeData[self.level]
                if upgrade then
                    if type(upgrade) == "number" then
                        self.level = upgrade
                    else
                        self.level = self.instance:randChoice(upgrade)
                    end
                end
            end
        end,

        --- !!! Internal
        --- @param self LibLevelGen.Entity
        upgrade = function(self)
            local enemyData = enemy.getData(self.type)
            if enemyData then
                if self.instance.runState.peaceRingActive then
                    self:processUpgrade(enemyData.peaceRingDowngrade)
                end
                if self.instance.runState.warRingActive then
                    self:processUpgrade(enemyData.warRingUpgrade)
                end
                if self.instance.runState.warShrineActive then
                    self:processUpgrade(enemyData.warShrineUpgrade)
                end
                if challangeCharacterActive(self.instance) then
                    self:processUpgrade(enemyData.challengeCharacterDowngrade)
                end
            end
        end,

        --- Set value of the given component field for when the entity is spawned.
        --- @param self LibLevelGen.Entity # The entity
        --- @param component string # Component name
        --- @param field string # Field in the given component
        --- @param value any # Value to write to the field
        --- @return LibLevelGen.Entity
        setAttribute = function (self, component, field, value)
            levelUtils.addEntityAttribute(self, component, field, value)
            return self
        end,

        --- !!! Internal
        --- @param self LibLevelGen.Entity
        --- @param currency? "coins" | "blood" | "diamonds"
        --- @param discount number | nil

        calculatePrice = function (self, currency, discount)
            -- cost multiplier is handled by the base game's handling of depthPriceMultiplier option
            currency = currency or "coins"
            local costComponent = ecs.getEntityPrototype(self:getRealType()).itemPrice
            if not costComponent then
                return specialCost[self.type] and (math.ceil(specialCost[self.type]) * (discount or 1)) or 0
            else
                return (math.ceil(costComponent[currency] * (discount or 1)))
            end
        end,

        --- Set price of the entity. If price is not specified, it is automatically obtained.
        --- @param self LibLevelGen.Entity # The Entity
        --- @param currency? "coins" | "blood" # Currency type, defaults to "coins"
        --- @param amount? number # The price value. If not specified, it's calculated automatically.
        --- @return LibLevelGen.Entity
        setPrice = function (self, currency, amount)
            self.saleCurrency = currency or "coins"
            if not amount then
                amount = self:calculatePrice(currency)
            end
            if self.saleCurrency == "coins" then
                levelUtils.setEntityPriceTag(self, nil, amount)
            else
                levelUtils.setEntityPriceTag(self, amount, nil)
            end

            return self
        end,

        --- Set discount on the price.
        --- @param self LibLevelGen.Entity # The entity
        --- @param discount number # The discount factor. `0.5` will result in the price being halved.
        --- @return LibLevelGen.Entity
        setDiscount = function(self, discount)
            local currentPrice = findAttribute(self.price.attributes, componentForCurrency[self.saleCurrency], "cost")
            if currentPrice then
                currentPrice.value = math.ceil(currentPrice.value * discount)
            else
                error("attempt to set discount on entity with no price")
            end

            return self
        end,

        --- Set quantity (itemStack.quantity field)
        --- @param self LibLevelGen.Entity # The entity
        --- @param quantity number # The quantity amount to set
        --- @return LibLevelGen.Entity
        setQuantity = function(self, quantity)
            return self:setAttribute("itemStack", "quantity", quantity)
        end,

        --- Add an item to entity's inventory.
        --- @param self LibLevelGen.Entity # The entity
        --- @param item string | LibLevelGen.Entity # Entity type or an existing entity to add to the inventory.
        --- @return LibLevelGen.Entity
        giveItem = function (self, item)
            if type(item) == "string" then
                item = entity.new(self.instance, item, self.x, self.y)
            end
            levelUtils.addEntityToInventory(self, item:serialize())

            return self
        end,

        --- Get an attribute of the entity. If it doesn't exist, it gets created and added (with no value specified)
        --- @param self LibLevelGen.Entity # The entity
        --- @param component string # Component name
        --- @param field string # Field name in the given component
        --- @return LibLevelGen.Entity
        getOrCreateAttribute = function (self, component, field)
            local attrs = self.attributes or {}
            for _, attr in ipairs(attrs) do
                if attr.component == component and attr.field == field then
                    return attr
                end
            end
            local attr = {
                component = component,
                field = field
            }
            attrs[#attrs + 1] = attr
            self.attributes = attrs
            return attr
        end,

        --- Add an item to entity's storage.
        --- @param self LibLevelGen.Entity # The entity
        --- @param item string # Item entity type
        --- @return LibLevelGen.Entity
        storeItem = function (self, item)
            local storage = self:getOrCreateAttribute("storage", "items")
            local items = storage.value or {}
            items[#items + 1] = item
            storage.value = items

            return self
        end,

        --- !!! Internal
        --- @param self LibLevelGen.Entity
        getRealType = function (self)
            return self.type .. (self.level > 1 and tostring(self.level) or "")
        end,

        --- !!! Internal
        --- @param self LibLevelGen.Entity
        serialize = function (self)
            return {
                type = self:getRealType(),
                x = self.x,
                y = self.y,
                attributes = self.attributes,
                inventory = self.inventory,
                price = self.price
            }
        end
    }
}

--- Create a new LibLevelGen entity. Do note that instead of using this, you should use methods provided by the `LibLevelGen.Room` and `LibLevelGen.Tile` classes, which will place the enemy in the level automatically.
--- @param instance LibLegelGen.Instance # Parent instance of the entity
--- @param type string # Entity type
--- @param x? number # X coordinate of the entity, relative to the room it's in. Defaults to -1
--- @param y? number # Y coordinate of the entity, relative to the room it's in Defaults to -1
--- @param level? number | number[] # Level of the entity, if a table is specified it's picked randomly from the provided values. Defaults to 1.
--- @return LibLevelGen.Entity
function entity.new(instance, type, x, y, level)
    local self = {}
    setmetatable(self, entityMt)
    self:init(instance, type, x, y, level)
    return self
end

return entity
