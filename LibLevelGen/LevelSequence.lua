local boss = require "necro.game.level.Boss"
local bossLevel = require "necro.game.data.level.BossLevel"
local proceduralLevel = require "necro.game.data.level.ProceduralLevel"
local dungeonLoader = require "necro.game.data.level.DungeonLoader"
local trainingLevel = require "necro.game.data.level.TrainingLevel"
local gameDLC = require "necro.game.data.resource.GameDLC"
local rng = require "necro.game.system.RNG"
local bultinLevelSequence = require "necro.game.level.LevelSequence"

local levelSequence = {}

--- @rawbegin levelSequence.BuiltinGenerators
-- A collection of built-in generator IDs.
levelSequence.BuiltinGenerators = {
    -- The default generator used by all-zones mode.
    NECROLEVEL = proceduralLevel.GENERATOR_TYPE,

    -- Generator used to generate boss floors.
    BOSS = bossLevel.GENERATOR_TYPE,

    -- Custom dungeon loader.
    CUSTOM_DUNGEON = dungeonLoader.GENERATOR_TYPE,

    -- Training level.
    TRAINING = trainingLevel.GENERATOR_TYPE,
}
--- @rawend

local function getRandomBossPool()
	return {
		boss.Type.KING_CONGA,
		boss.Type.DEATH_METAL,
		boss.Type.DEEP_BLUES,
		boss.Type.CORAL_RIFF,
	}
end

--- Level sequence which ends every zone with a random boss (the boss generator is automatically invoked).
--- @return fun(ev: Event.LevelSequenceUpdate)
function levelSequence.templateAllZones()
    return function (ev, cfg)
        local bossTypes = getRandomBossPool()
        if gameDLC.isAmplifiedLoaded() then
            bossTypes[#bossTypes + 1] = boss.Type.FORTISSIMOLE
        end
        bossTypes = rng.shuffle(bossTypes, ev.rng)
        for zone = 1, cfg.zones do
            for floor = 1, cfg.levelsPerZone - 1 do
                ev.sequence[#ev.sequence + 1] = {
                    zone = zone,
                    depth = zone,
                    floor = floor
                }
            end
            ev.sequence[#ev.sequence + 1] = {
                type = levelSequence.BuiltinGenerators.BOSS,
                boss = bossTypes[zone],
                zone = zone,
                depth = zone,
                floor = cfg.levelsPerZone
            }
        end
    end
end

--- Basic level sequence which simply has a given amount of levels (based on maxLevel in generator config).
--- @return fun(ev: Event.LevelSequenceUpdate)
function levelSequence.templateBasic()
    return function (ev, cfg)
        for i = 1, cfg.maxLevel do
            ev.sequence[#ev.sequence + 1] = {
                zone = 1,
                depth = 1,
                floor = i
            }
        end
    end
end

return levelSequence
