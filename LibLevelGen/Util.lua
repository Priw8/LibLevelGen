local collision = require "necro.game.tile.Collision"
local tileTypes = require "necro.game.tile.TileTypes"

local levelGenUtil = {}

--- @rawbegin levelGenUtil.TileRequirements  helper table
levelGenUtil.TileRequirements = {
    Enemy = {
        Generic = {
            isFloor = true,
            levelExit = false,
            hasEntity = false,
            adjacentEntrance = false,
            notCollision = collision.Group.UNSAFE,
        },
        NotNearWall = {
            isFloor = true,
            levelExit = false,
            hasEntity = false,
            nearWall = false,
            adjacentEntrance = false,
            notCollision = collision.Group.UNSAFE,
        },
        MovingSlime = {
            isFloor = true,
            levelExit = false,
            hasEntity = false,
            nearWall = false,
            adjacentEntrance = false,
            notCollision = collision.Group.UNSAFE,
        },
        OnWall = {
            hasEntity = false,
            isWall = true,
        },
        OnWire = {
            isFloor = true,
            levelExit = false,
            hasEntity = false,
            adjacentEntrance = false,
            notCollision = collision.Group.UNSAFE,
            wire = true
        }
    },
    Trap = {
        Generic = {
            isFloor = true,
            levelExit = false,
            hasEntity = false,
            adjacentEntrance = false,
            notCollision = collision.Group.UNSAFE,
            wire = false,
            noWallTallSprite = true
        }
    },
    Shrine = {
        Generic = {
            isFloor = true,
            levelExit = false,
            hasEntity = false,
            adjacentEntrance = false,
            notCollision = collision.Group.UNSAFE,
            nearWall = false,
            wire = false
        }
    },
    Terrain = {
        Hazard = {
            isFloor = true,
            adjacentEntrance = false,
            notCollision = collision.Group.UNSAFE,
            levelExit = false
        },
        Exit = {
            isFloor = true,
            adjacentEntrance = false,
        }
    },
    Loot = {
        Generic = {
            isFloor = true,
            hasEntity = false,
            adjacentEntrance = false,
            notCollision = collision.Group.UNSAFE,
            wire = false,
            levelExit = false
        },
        Chest = {
            isFloor = true,
            hasEntity = false,
            adjacentEntrance = false,
            notCollision = collision.Group.UNSAFE,
            wire = false,
            levelExit = false
        }
    },
}
--- @rawend

return levelGenUtil
