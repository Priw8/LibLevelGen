local enum = require "system.utils.Enum"
local utils = require "system.utils.Utilities"

local rand = require "LibLevelGen.Rand"

local shrine = {}

-- From ndref
--[[
    Const SHRINE_BLOOD: Int = 0
    Const SHRINE_DARKNESS: Int = 1
    Const SHRINE_GLASS: Int = 2
    Const SHRINE_PEACE: Int = 3
    Const SHRINE_RHYTHM: Int = 4
    Const SHRINE_RISK: Int = 5
    Const SHRINE_SACRIFICE: Int = 6
    Const SHRINE_SPACE: Int = 7
    Const SHRINE_WAR: Int = 8
    Const SHRINE_NO_RETURN: Int = 9
    Const SHRINE_PHASING: Int = 10
    Const SHRINE_PACE: Int = 11
    Const SHRINE_CHANCE: Int = 12
    Const SHRINE_BOSS: Int = 13
    Const SHRINE_PAIN: Int = 14
    Const SHRINE_UNCERTAINTY: Int = 15
]]--

--- TEST SHRINE ENUM COMMENT
shrine.ID = enum.sequence {
    BLOOD = 0,
    DARKNESS = 1,
    GLASS = 2,
    PEACE = 3,
    RHYTHM = 4,
    RISK = 5,
    SACRIFICE = 6,
    SPACE = 7,
    WAR = 8,
    NO_RETURN = 9,
    PHASING = 10,
    PACE = 11,
    CHANCE = 12,
    BOSS = 13,
    PAIN = 14,
    UNCERTAINTY = 15
}

--- @class LibLevelGen.Shrine
--- @field name string
--- @field xmlID number
--- @field old boolean
--- @field notAllowedInShriner boolean

--- @param args LibLevelGen.Shrine
local function createShrineData(args)
    if not args.name then
        error("shrine must have a name provided")
    end
    args.xmlID = args.xmlID or shrine.ID.extend(args.name)
    args.old = args.old == true
    args.notAllowedInShriner = args.notAllowedInShriner ~= true

    return enum.data(args)
end

--- @type table<string,number>
shrine.Type = enum.sequence {
    BLOOD = createShrineData{id = shrine.ID.BLOOD, name = "ShrineOfBlood"},
    DARKNESS = createShrineData{id = shrine.ID.DARKNESS, name = "ShrineOfDarkness"},
    GLASS = createShrineData{id = shrine.ID.GLASS, name = "ShrineOfGlass"},
    PEACE = createShrineData{id = shrine.ID.PEACE, name = "ShrineOfPeace"},
    RHYTHM = createShrineData{id = shrine.ID.RHYTHM, name = "ShrineOfRhythm"},
    RISK = createShrineData{id = shrine.ID.RISK, name = "ShrineOfRisk"},
    SACRIFICE = createShrineData{id = shrine.ID.SACRIFICE, name = "ShrineOfSacrifice", notAllowedInShriner = true},
    SPACE = createShrineData{id = shrine.ID.SPACE, name = "ShrineOfSpace"},
    WAR = createShrineData{id = shrine.ID.WAR, name = "ShrineOfWar"},
    NO_RETURN = createShrineData{id = shrine.ID.NO_RETURN, name = "ShrineOfNoReturn", old = true},
    PHASING = createShrineData{id = shrine.ID.PHASING, name = "ShrineOfPhasing", old = true},
    PACE = createShrineData{id = shrine.ID.PACE, name = "ShrineOfPace", old = true},
    CHANCE = createShrineData{id = shrine.ID.CHANCE, name = "ShrineOfChance", notAllowedInShriner = true},
    BOSS = createShrineData{id = shrine.ID.BOSS, name = "ShrineOfBoss"},
    PAIN = createShrineData{id = shrine.ID.PAIN, name = "ShrineOfPain", notAllowedInShriner = true},
    UNCERTAINTY = createShrineData{id = shrine.ID.UNCERTAINTY, name = "ShrineOfUncertainty"},
}

--- @param seenShrines number[]
--- @param generatorData LibLevelGen.Config
--- @param rng LibLevelGen.Rng
--- @return table<number,number>
function shrine.createShrineLevelNumbers(seenShrines, generatorData, rng)
    local numberOfZones = generatorData.maxLevel / generatorData.levelsPerZone
    local res = {}

    local shrineTypes = shrine.getElligibleShrines(seenShrines, generatorData)

    for zone = 1, numberOfZones do
        local shrineInThisZone = rng:randChoice(shrineTypes, true)
        shrine.markSeen(shrineInThisZone, seenShrines)
        local minLvl = (zone ~= 1 or generatorData.allowLevel1Shrine) and 1 or 2
        local levelNumber = (zone-1)*generatorData.levelsPerZone + rng:randIntRange(minLvl, generatorData.levelsPerZone)
        -- NaN check
        if levelNumber == levelNumber then
            res[levelNumber] = shrineInThisZone
        else
            log.warn("Failed to generate level for shrine: " .. shrineInThisZone)
        end
    end

    return res
end

--- @param seenShrines number[]
--- @param generatorData LibLevelGen.Config
--- @return number[]
function shrine.getElligibleShrines(seenShrines, generatorData)
    local shrineTypes = utils.getValueList(shrine.Type)

    if not generatorData.allowOldShrines then
        utils.removeIf(shrineTypes, function(shrineType)
            local shrineData = shrine.Type.data[shrineType]
            return shrineData.old == true
        end)
    end
    utils.removeIf(shrineTypes, function(shrineType)
        return shrine.checkWasSeen(shrineType, seenShrines)
    end)

    return shrineTypes
end

--- @param shrineType number
--- @param seenShrines number[]
function shrine.markSeen(shrineType, seenShrines)
    seenShrines[#seenShrines+1] = shrineType
end

--- @param shrineType number
--- @param seenShrines number[]
--- @return boolean
function shrine.checkWasSeen(shrineType, seenShrines)
    for i = 1, #seenShrines do
        if seenShrines[i] == shrineType then
            return true
        end
    end
    return false
end

--- @param name string
--- @param args LibLevelGen.Shrine
--- @return number
function shrine.register(name, args)
    args.name = name
    return shrine.Type.extend(args.name, createShrineData(args))
end

--- @param type string
--- @return LibLevelGen.Shrine
function shrine.getData(type)
    return shrine.Type.data[type]
end

return shrine
