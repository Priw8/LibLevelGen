local currency = require "necro.game.item.Currency"
local itemGeneration = require "necro.game.item.ItemGeneration"

local loot = require "LibLevelGen.Loot"
local levelGenUtil = require "LibLevelGen.Util"
local room = require "LibLevelGen.Room"

local tr = levelGenUtil.TileRequirements

local secretRoom = {}

--- Finds suitable locations for secret rooms and places them.
--- @param segment LibLevelGen.Segment
--- @return LibLevelGen.Room[]
function secretRoom.placeSecretRooms(segment)
    -- TODO: I'm sure this can be done in a better way lol
    local tileset = segment.tileset
    local secretRooms = {}
    local betweenRects = segment:findBetweenRoomRects(3, 3)
    for i = 1, #betweenRects do
        local rect = betweenRects[i]
        if
            segment:beetweenRoomRectSidesWithAdjecentRooms(rect) >= 2 and
            rect.w < 8 and rect.h < 8
        then
            local validAlignmentTypes = {
                TOP_LEFT = 0,
                TOP_RIGHT = 0,
                BOTTOM_LEFT = 0,
                BOTTOM_RIGHT = 0
            }
            if #rect.overlaps.top > 0 then
                validAlignmentTypes.TOP_LEFT = validAlignmentTypes.TOP_LEFT + 1
                validAlignmentTypes.TOP_RIGHT = validAlignmentTypes.TOP_RIGHT + 1
            end
            if #rect.overlaps.bottom > 0 then
                validAlignmentTypes.BOTTOM_LEFT = validAlignmentTypes.BOTTOM_LEFT + 1
                validAlignmentTypes.BOTTOM_RIGHT = validAlignmentTypes.BOTTOM_RIGHT + 1
            end
            if #rect.overlaps.left > 0 then
                validAlignmentTypes.TOP_LEFT = validAlignmentTypes.TOP_LEFT + 1
                validAlignmentTypes.BOTTOM_LEFT = validAlignmentTypes.BOTTOM_LEFT + 1
            end
            if #rect.overlaps.right > 0 then
                validAlignmentTypes.TOP_RIGHT = validAlignmentTypes.TOP_RIGHT + 1
                validAlignmentTypes.BOTTOM_RIGHT = validAlignmentTypes.BOTTOM_RIGHT + 1
            end

            local elligibleAlignmentTypes = {}
            for alignment, value in pairs(validAlignmentTypes) do
                if value > 1 then
                    elligibleAlignmentTypes[#elligibleAlignmentTypes+1] = alignment
                end
            end

            local alignmentType = segment.instance:randChoice(elligibleAlignmentTypes)
            if alignmentType ~= nil then

                local w = rect.w > 3 and segment.instance:randIntRange(2, 4) or 2
                local h = rect.h > 3 and segment.instance:randIntRange(2, 4) or 2

                local fillRect
                if alignmentType == "TOP_LEFT" then
                    fillRect = {
                        x = 0,
                        y = 0,
                        w = w,
                        h = h
                    }
                elseif alignmentType == "TOP_RIGHT" then
                    fillRect = {
                        x = rect.w - w,
                        y = 0,
                        w = w,
                        h = h
                    }
                elseif alignmentType == "BOTTOM_LEFT" then
                    fillRect = {
                        x = 0,
                        y = rect.h - h,
                        w = w,
                        h = h
                    }
                elseif alignmentType == "BOTTOM_RIGHT" then
                    fillRect = {
                        x = rect.w - w,
                        y = rect.h - h,
                        w = w,
                        h = h
                    }
                end

                local secretRoom = segment:createRoom(fillRect.x + rect.x, fillRect.y + rect.y, fillRect.w, fillRect.h, room.Type.SECRET)
                secretRoom:fill {
                    tileset = tileset,
                    tileType = "Floor",
                }
                secretRoom.fillRect = fillRect
                secretRooms[#secretRooms+1] = secretRoom
            end
        end
    end
    return secretRooms
end

--- @param currentRoom LibLevelGen.Room
local function fillSecretRoomsDefault(currentRoom)
    local segment = currentRoom.segment
    local instance = currentRoom.instance
    local variantRoll = segment.instance:randIntRange(0, 101)
    if variantRoll <= 4 and currentRoom.w == 2 and currentRoom.h == 2 then
        secretRoom.makeCrateRoom(currentRoom)
    elseif variantRoll <= 5 and currentRoom.w == 3 and currentRoom.h == 3 then
        secretRoom.makeMouseTrap(currentRoom)
    elseif variantRoll > 25 then
        if variantRoll <= 45 then
            secretRoom.makeTrapRoom(currentRoom)
        else
            -- There's an elseif that makes a room with a spirit before this
            -- else normally, but I am not spawning spirits from a built-in function.
            -- Screw that.
            variantRoll = segment.instance:randIntRange(0, 101)
            if variantRoll <= 30 then
                secretRoom.makeBatCave(currentRoom)
            elseif variantRoll <= 35 then
                secretRoom.makeSkeletonCloset(currentRoom)
            elseif variantRoll <= 75 then
                secretRoom.placeBombOrGold(currentRoom)
            end
        end
    else
        if instance:randChance(1/41) and not instance.runState.urnPlaced then
            secretRoom.placeUrn(currentRoom)
        elseif instance:randChance(1/5) and currentRoom.segment.chestsLeft > 0 then
            secretRoom.placeHiddenChest(currentRoom)
        elseif currentRoom.segment.chestsLeft > 0 then
            secretRoom.placeChest(currentRoom)
        end
    end
end

--- @param currentRoom LibLevelGen.Room
local function fillSecretRoomsZone2(currentRoom)
    local segment = currentRoom.segment
    local instance = currentRoom.instance
    local variantRoll = segment.instance:randIntRange(0, 101)
    if variantRoll <= 4 and currentRoom.w == 2 and currentRoom.h == 2 then
        secretRoom.makeCrateRoom(currentRoom)
    elseif variantRoll <= 5 and currentRoom.h == 3 and currentRoom.h == 3 then
        secretRoom.makeMouseTrap(currentRoom)
    elseif variantRoll > 25 then
        if variantRoll <= 45 then
            secretRoom.makeTrapRoom(currentRoom)
        elseif variantRoll <= 95 then
            secretRoom.placeBombOrGold(currentRoom)
        end
    else
        if instance:randChance(1/41) and not instance.runState.urnPlaced then
            secretRoom.placeUrn(currentRoom)
        elseif instance:randChance(1/5) and currentRoom.segment.chestsLeft > 0 then
            secretRoom.placeHiddenChest(currentRoom)
        elseif currentRoom.segment.chestsLeft > 0 then
            secretRoom.placeChest(currentRoom)
        end
    end
end

--- @param currentRoom LibLevelGen.Room
local function fillSecretRoomsZone4(currentRoom)
    local segment = currentRoom.segment
    local instance = currentRoom.instance
    local variantRoll = segment.instance:randIntRange(0, 101)
    if variantRoll <= 4 and currentRoom.w == 2 and currentRoom.h == 2 then
        secretRoom.makeCrateRoom(currentRoom)
    elseif variantRoll <= 5 and currentRoom.h == 3 and currentRoom.h == 3 then
        secretRoom.makeMouseTrap(currentRoom)
    elseif variantRoll > 25 then
        if variantRoll <= 45 then
            secretRoom.makeTrapRoom(currentRoom)
        else
            -- There's an elseif that makes a room with a spirit before this
            -- else normally, but I am not spawning spirits from a built-in function.
            -- Screw that.
            variantRoll = segment.instance:randIntRange(0, 101)
            if variantRoll <= 30 then
                secretRoom.makeBatCave(currentRoom, 4)
            elseif variantRoll <= 35 then
                secretRoom.makeSkeletonCloset(currentRoom)
            elseif variantRoll <= 75 then
                secretRoom.placeBombOrGold(currentRoom)
            end
        end
    else
        if instance:randChance(1/41) and not instance.runState.urnPlaced then
            secretRoom.placeUrn(currentRoom)
        elseif instance:randChance(1/5) and currentRoom.segment.chestsLeft > 0 then
            secretRoom.placeHiddenChest(currentRoom)
        elseif currentRoom.segment.chestsLeft > 0 then
            secretRoom.placeChest(currentRoom)
        end
    end
end

local zoneFillTypes = {
    [2] = fillSecretRoomsZone2,
    [4] = fillSecretRoomsZone4
}

--- Fills secret rooms in the same way necrolevel does.
--- @param segment LibLevelGen.Segment
--- @param fillType number? unused, reserved
function secretRoom.fillSecretRooms(segment, fillType)
    local instance = segment.instance
    -- TODO: potion/vault
    local fillFunc = zoneFillTypes[fillType] or fillSecretRoomsDefault
    local secretRooms = segment:selectRooms(room.Flag.ALLOW_SECRET_FILL)
    for _, currentRoom in ipairs(secretRooms) do
        if segment.forceSecretRoomBomb then
            -- TODO: bombless char check
            currentRoom:placeEntityRand(tr.Loot.Generic, instance:randChance(0.45) and "Bomb3" or "Bomb")
            segment.forceSecretRoomBomb = false
        else
            fillFunc(currentRoom)
        end
    end

    if segment.forceSecretRoomBomb then
        -- If still set here, then there were no secret rooms to place the bomb...
        -- TODO: optionally make this a failmap?
        log.warn("secretRoom.fillSecretRooms: unable to place the bomb for locked shop")
    end
end

local mimicChances = {3, 9, 12}
--- @param room LibLevelGen.Room
function secretRoom.placeChest(room)
    local instance = room.instance
    local chestMimicRoll = instance:randIntRange(0, 100)
    local mimicChance = mimicChances[instance:getFloor()] or 15
    if chestMimicRoll <= mimicChance then
        local mimicLevelRoll = instance:randIntRange(0, 10)
        local mimicLevel = 1
        if mimicLevelRoll == 0 then
            mimicLevel = 3
        elseif mimicLevelRoll == 1 then
            mimicLevel = 2
        end
        local chest = loot.createChest(instance, {
            itemPool = mimicLevel > 1 and itemGeneration.Pool.LOCKED_CHEST or itemGeneration.Pool.CHEST,
            chestType = "TrapChest",
            chestLevel = mimicLevel,
            levelBonus = 1
        })

        room:placeEntityRand(tr.Loot.Chest, chest)
    else
        room:placeEntityRand(tr.Loot.Chest, loot.createChest(instance))
    end
    room.segment.chestsLeft = room.segment.chestsLeft - 1
end

--- @param room LibLevelGen.Room
function secretRoom.placeHiddenChest(room)
    local chest = loot.createChest(room.instance, {
        itemPool = itemGeneration.Pool.CHEST,
        levelBonus = 3,
        hidden = true
    })
    room:placeEntityRand(tr.Loot.Chest, chest)
    room.segment.chestsLeft = room.segment.chestsLeft - 1
end

--- @param room LibLevelGen.Room
function secretRoom.makeCrateRoom(room)
    local lootTiles = room:selectTiles(tr.Loot.Generic)
    for _, lootTile in ipairs(lootTiles) do
        lootTile:placeEntity(loot.createCrate(room.instance))
    end
end

--- @param room LibLevelGen.Room
function secretRoom.makeMouseTrap(room)
    local instance = room.instance

    -- Mouse trap
    -- Traps to place (clockwise, starting from 12:00)
    -- TODO: these might need to be placed in same order as necrolevel? for enemy interactions
    local trapTypes = instance:randChoice{
        {"BounceTrapDown", "BounceTrapLeft", "BounceTrapUp", "BounceTrapRight"},
        {"SpikeTrap", "SpikeTrap", "SpikeTrap", "SpikeTrap"},
        {"Trapdoor", "Trapdoor", "Trapdoor", "Trapdoor"}
    }
    room:placeEntityAt(1, 0, trapTypes[1])
    room:placeEntityAt(2, 1, trapTypes[2])
    room:placeEntityAt(1, 2, trapTypes[3])
    room:placeEntityAt(0, 1, trapTypes[4])

    if instance:randChance(0.5) then
        room:placeEntityAt(1, 1, itemGeneration.choice {itemPool = itemGeneration.Pool.CHEST, levelBonus = 1})
    else
        room:placeEntityAt(1, 1, itemGeneration.choice {itemPool = itemGeneration.Pool.FOOD})
    end
end

--- @param room LibLevelGen.Room
function secretRoom.makeTrapRoom(room)
    local instance = room.instance

    -- Place the corner traps
    if instance:randChance(0.5) then
        room:placeEntityAt(0, 0, instance:randChoice{"BounceTrapDown", "BounceTrapRight"})
        room:placeEntityAt(room.w-1, room.h-1, instance:randChoice{"BounceTrapUp", "BounceTrapLeft"})
    else
        room:placeEntityAt(room.w-1, 0, instance:randChoice{"BounceTrapDown", "BounceTrapLeft"})
        room:placeEntityAt(0, room.h-1, instance:randChoice{"BounceTrapUp", "BounceTrapRight"})
    end

    local trapTilesLeft = room:selectTiles(tr.Trap.Generic)
    for _, trapTile in ipairs(trapTilesLeft) do
        -- Ported from necrolevel
        if instance:randIntRange(0, 11) == 0 then
            trapTile:placeEntity("BombTrap")
        else
            if instance:randChance(0.25) then
                trapTile:placeEntity("Trapdoor")
            else
                trapTile:placeEntity("SpikeTrap")
            end
        end
    end
end

--- @param room LibLevelGen.Room
--- @param secretBatLevel number?
function secretRoom.makeBatCave(room, secretBatLevel)
    local instance = room.instance

    local placeChest = instance:randChance(0.2)
    local batLevel = placeChest and (secretBatLevel or 2) or 1
    if placeChest and room.segment.chestsLeft >= 1 then
        -- Place (hidden) chest
        room.segment.chestsLeft = room.segment.chestsLeft - 1
        room:placeEntityRand(tr.Loot.Chest, loot.createChest(room.instance, {
            itemPool = itemGeneration.Pool.CHEST,
            levelBonus = 3,
            hidden = true
        }))
    end

    local batTiles = room:selectTiles(tr.Enemy.Generic)
    for _, batTile in ipairs(batTiles) do
        batTile:placeEntity("Bat", batLevel)
    end
end

--- @param room LibLevelGen.Room
function secretRoom.makeSkeletonCloset(room)
    local instance = room.instance

    local skeletonLevel = 1
    skeletonLevel = instance:randChance(1/3) and 2 or skeletonLevel
    skeletonLevel = instance:randChance(1/4) and 3 or skeletonLevel

    local skeletonTiles = room:selectTiles(tr.Enemy.Generic)
    for _, skeletonTile in ipairs(skeletonTiles) do
        skeletonTile:placeEntity("Skeleton", skeletonLevel)
    end
end

--- @param room LibLevelGen.Room
function secretRoom.placeBombOrGold(room)
    local variantRoll = room.instance:randIntRange(0, 101)
    -- TODO: bombless char check
    if variantRoll <= 34 then
        room:placeEntityRand(tr.Loot.Generic, "Bomb3")
    elseif variantRoll >= 80 then
        local amount = math.min(room.instance:getDepth(), 3) * 10 + 15
        local gold = loot.createGold(room.instance, amount)
        room:placeEntityRand(tr.Loot.Generic, gold)
    else
        room:placeEntityRand(tr.Loot.Generic, "Bomb")
    end
end

--- @param room LibLevelGen.Room
function secretRoom.placeUrn(room)
    local tehUrn = loot.createUrn(room.instance)
    room:placeEntityRand(tr.Loot.Generic, tehUrn)
    room.instance.runState.urnPlaced = true
end

return secretRoom
