local enum = require "system.utils.Enum"

local enemy = {}

-- These match the Necrodancer.xml IDS, for interoperability with Necrolevel
minibossIDs = enum.sequence {
    DIREBAT1 = 400,
    DIREBAT2 = 401,
    DRAGON1 = 402,
    DRAGON2 = 403,
    DRAGON3 = 404,
    DRAGON4 = 415,
    BANSHEE1 = 405,
    BANSHEE2 = 406,
    MINOTAUR1 = 407,
    MINOTAUR2 = 408,
    NIGHTMARE1 = 409,
    NIGHTMARE2 = 410,
    MOMMY = 411,
    OGRE = 412,
    METROGNOME1 = 413,
    METROGNOME2 = 414,
}

-- TODO: rewrite upgrades/downgrades to use built-in EnemySubsitutions module

--- @class LibLevelGen.EnemyData
--- @field entity string # The base entity type
--- @field maxLevel number # Max level the entity type can have
--- @field id number[] # Numeric IDs for every level the entity has
--- @field warRingUpgrade table<number,number> # Table containing a mapping which specifies which levels get converted to which when ring of war is in effect
--- @field warShrineUpgrade table<number,number> # Table containing a mapping which specifies which levels get converted to which when shrine of war is in effect
--- @field peaceRingDowngrade table<number,number> # Table containing a mapping which specifies which levels get converted to which when ring of peace is in effect
--- @field challengeCharacterDowngrade table<number,number> # Table containing a mapping which specifies which levels get converted to which when playing a challenge character (Monk/Coda/Aria/Bolt - used to downgrade black bats into blue)
--- Class containing data related to processing it by the various level generation components.


--- Automatically generate numeric IDs for every level of the enemy type provided
--- @param args LibLevelGen.EnemyData
--- @return number[]
local function createEnemyIDs(args)
    local id = {}

    for i = 1, args.maxLevel do
        id[i] = minibossIDs.extend(args.entity .. tostring(i))
    end

    return id
end

--- @param args LibLevelGen.EnemyData
--- @return table<number,number>
local function createDefaultWarShrineUpgrade(args)
    local res = {}
    for i = 1, args.maxLevel - 1 do
        res[i] = args.maxLevel
    end
    return res
end

--- @param entity string
--- @param maxLevel number?
--- @param args LibLevelGen.EnemyData?
--- @return LibLevelGen.EnemyData
local function createEnemyData(entity, maxLevel, args)
    args = args or {}

    args.entity = entity
    args.maxLevel = maxLevel or 1
    args.id = args.id or createEnemyIDs(args)

    args.warRingUpgrade = args.warRingUpgrade or {}
    args.warShrineUpgrade = args.warShrineUpgrade or createDefaultWarShrineUpgrade(args)
    args.peaceRingDowngrade = args.peaceRingDowngrade or {}
    -- For Aria/Bolt/Coda bats
    args.challengeCharacterDowngrade = args.challengeCharacterDowngrade or {}

    return enum.data(args)
end

--- Enum containing enemy types and their associated data. Use `enemy.getData` to get data for a given enemy from this enum.
--- @type table<string,number>
enemy.Type = enum.sequence {
    -- Bosses
    King = createEnemyData("King", 2),
    CoralRiff = createEnemyData("CoralRiff"),
    KingConga = createEnemyData("KingConga"),
    Deathmetal = createEnemyData("Deathmetal"),
    Fortissimole = createEnemyData("Fortissimole"),
    Necrodancer = createEnemyData("Necrodancer", 2, {
        warShrineUpgrade = {}
    }),
    DeadRinger = createEnemyData("DeadRinger"),
    Frankensteinway = createEnemyData("Frankensteinway"),
    Conductor = createEnemyData("Conductor"),
    LuteDragon = createEnemyData("LuteDragon"),

    -- Minibosses
    Direbat = createEnemyData("BatMiniboss", 2, {
        id = {
            [1] = minibossIDs.DIREBAT1,
            [2] = minibossIDs.DIREBAT2
        },
        warRingUpgrade = {
            [1] = 2
        },
        warShrineUpgrade = {
            [1] = 2
        },
        peaceRingDowngrade = {
            [2] = 1
        }
    }),
    Dragon = createEnemyData("Dragon", 4, {
        id = {
            [1] = minibossIDs.DRAGON1,
            [2] = minibossIDs.DRAGON2,
            [3] = minibossIDs.DRAGON3,
            [4] = minibossIDs.DRAGON4
        },
        warRingUpgrade = {
            [1] = 2,
            [3] = 2,
            [4] = 2,
        },
        warShrineUpgrade = {
            [1] = {2, 3},
        },
        peaceRingDowngrade = {
            [2] = 1,
            [3] = 1,
            [4] = 1
        }
    }),
    Banshee = createEnemyData("Banshee", 2, {
        id = {
            [1] = minibossIDs.BANSHEE1,
            [2] = minibossIDs.BANSHEE2
        },
        warRingUpgrade = {
            [1] = 2
        },
        warShrineUpgrade = {
            [1] = 2
        },
        peaceRingDowngrade = {
            [2] = 1
        }
    }),
    Minotaur = createEnemyData("Minotaur", 2, {
        id = {
            [1] = minibossIDs.MINOTAUR1,
            [2] = minibossIDs.MINOTAUR2,
        },
        warRingUpgrade = {
            [1] = 2
        },
        warShrineUpgrade = {
            [1] = 2
        },
        peaceRingDowngrade = {
            [2] = 1
        }
    }),
    Nightmare = createEnemyData("Nightmare", 2, {
        id = {
            [1] = minibossIDs.NIGHTMARE1,
            [2] = minibossIDs.NIGHTMARE2,
        },
        warRingUpgrade = {
            [1] = 2
        },
        warShrineUpgrade = {
            [1] = 2
        },
        peaceRingDowngrade = {
            [2] = 1
        }
    }),
    Mommy = createEnemyData("Mommy", 1, {
        id = {
            [1] = minibossIDs.MOMMY
        },
    }),
    Ogre = createEnemyData("Ogre", 1, {
        id = {
            [1] = minibossIDs.OGRE,
        },
    }),
    Metrognome = createEnemyData("Metrognome", 2, {
        id = {
            [1] = minibossIDs.METROGNOME1,
            [2] = minibossIDs.METROGNOME2
        },
        warRingUpgrade = {
            [1] = 2
        },
        warShrineUpgrade = {
            [1] = 2
        },
        peaceRingDowngrade = {
            [2] = 1
        }
    }),

    -- Regular enemies (alphabetically)
    Armadillo = createEnemyData("Armadillo", 3, {
        warShrineUpgrade = {
            [1] = 2
        }
    }),
    Armoredskeleton = createEnemyData("Armoredskeleton", 4, {
        warShrineUpgrade = {
            [1] = 3,
            [2] = 3
        }
    }),
    Bat = createEnemyData("Bat", 4, {
        warShrineUpgrade = {
            [1] = 2
        },
        challengeCharacterDowngrade = {
            [2] = 1,
            [4] = 1
        },
    }),
    Beetle = createEnemyData("Beetle", 2, {
        warShrineUpgrade = {}
    }),
    Bishop = createEnemyData("Bishop", 2),
    Blademaster = createEnemyData("Blademaster", 2),
    Cauldron = createEnemyData("Cauldron", 2, {
        warShrineUpgrade = {}
    }),
    Clone = createEnemyData("Clone"),
    Devil = createEnemyData("Devil", 2),
    ElectricMage = createEnemyData("ElectricMage", 3),
    Evileye = createEnemyData("Evileye", 2),
    Fireelemental = createEnemyData("Fireelemental"),
    Iceelemental = createEnemyData("Iceelemental"),
    Ghast = createEnemyData("Ghast"),
    Ghost = createEnemyData("Ghost"),
    Ghoul = createEnemyData("Ghoul"),
    Goblin = createEnemyData("Goblin", 2),
    GoblinBomber = createEnemyData("GoblinBomber"),
    Golem = createEnemyData("Golem", 3, {
        warShrineUpgrade = {
            [1] = 2
        }
    }),
    Gorgon = createEnemyData("Gorgon", 2),
    Harpy = createEnemyData("Harpy"),
    Hellhound = createEnemyData("Hellhound"),
    Knight = createEnemyData("Knight", 2),
    Lich = createEnemyData("Lich", 3),
    Monkey = createEnemyData("Monkey", 4, {
        warShrineUpgrade = {
            [1] = 2,
            [3] = 4
        }
    }),
    Mushroom = createEnemyData("Mushroom", 2),
    Orc = createEnemyData("Orc", 3),
    Pawn = createEnemyData("Pawn", 2),
    Pixie = createEnemyData("Pixie"),
    Queen = createEnemyData("Queen", 2),
    Rook = createEnemyData("Rook", 2),
    Sarcophagus = createEnemyData("Sarcophagus", 3),
    Shovemonster = createEnemyData("Shovemonster", 2),
    Skeleton = createEnemyData("Skeleton", 4, {
        warShrineUpgrade = {
            [1] = 3,
            [2] = 3
        }
    }),
    Skeletonknight = createEnemyData("Skeletonknight", 4, {
        warShrineUpgrade = {
            [1] = 3,
            [2] = 3
        }
    }),
    Skeletonmage = createEnemyData("Skeletonmage", 3),
    Skull = createEnemyData("Skull", 4, {
        warShrineUpgrade = {
            [1] = 3,
            [2] = 3
        }
    }),
    SleepingGoblin = createEnemyData("SleepingGoblin"),
    Slime = createEnemyData("Slime", 8, {
        warShrineUpgrade = {
            [1] = {2, 3}
        }
    }),
    Spider = createEnemyData("Spider"),
    Tarmonster = createEnemyData("Tarmonster"),
    Tentacle = createEnemyData("Tentacle", 8, {
        warShrineUpgrade = {
            [1] = 5,
            [2] = 6,
            [3] = 7,
            [4] = 8
        }
    }),
    Trapcouldron = createEnemyData("Trapcouldron", 2, {
        warShrineUpgrade = {}
    }),
    Trapchest = createEnemyData("Trapchest", 6, {
        warShrineUpgrade = {}
    }),
    Troll = createEnemyData("Troll"),
    Warlock = createEnemyData("Warlock", 2),
    WaterBall = createEnemyData("WaterBall", 2, {
        warShrineUpgrade = {}
    }),
    Wight = createEnemyData("Wight"),
    Wraith = createEnemyData("Wraith", 2, {
        warShrineUpgrade = {}
    }),
    Yeti = createEnemyData("Yeti"),
    Zombie = createEnemyData("Zombie"),
    ZombieElectric = createEnemyData("ZombieElectric"),
    ZombieSnake = createEnemyData("ZombieSnake"),
}

--- Gets LibLevelGen data for the given enemy type.
--- @param type string # Type from the enemy.Type enum
--- @return LibLevelGen.EnemyData
function enemy.getData(type)
    return enemy.Type.data[enemy.Type[type]]
end

--- Register LibLevelGen enemy data in the enemy enum.
--- @param entity string # Entity base name
--- @param maxLevel number # Max level this entity can have
--- @param args LibLevelGen.EnemyData # Additional enemy definition data
--- @return number
function enemy.register(entity, maxLevel, args)
    return enemy.Type.extend(entity, createEnemyData(entity, maxLevel, args))
end


return enemy