local itemSlot = require "necro.game.item.ItemSlot"
local levelSequence = require "necro.game.level.LevelSequence"
local enum = require "system.utils.Enum"
local utils = require "system.utils.Utilities"
local itemGeneration = require "necro.game.item.ItemGeneration"

local rand = require "LibLevelGen.Rand"
local shrine = require "LibLevelGen.Shrine"

local secretShop = {}

local function createTransmogrifier(room)
    -- that was easy
    room:placeEntityAt(3, 3, "Transmogrifier")
end

--- @param room LibLevelGen.Room
local function createArena(room)
    room:border {
        sides = {
            bottom = 1
        },
        rect = {
            x = 0,
            y = 0,
            w = room.w,
            h = 10
        },
        tileType = "ShopWall"
    }

    room:border {
        sides = {
            bottom = 2
        },
        tileType = "Void"
    }

    local instance = room.instance

    local itemType1 = itemGeneration.choice{itemPool = itemGeneration.Pool.RED_CHEST, levelBonus = 2}
    local itemType2 = itemGeneration.choice{itemPool = itemGeneration.Pool.PURPLE_CHEST, levelBonus = 2}
    local itemType3 = itemGeneration.choice{itemPool = itemGeneration.Pool.BLACK_CHEST, levelBonus = 2}

    local singleChoiceGroup = instance:getNewSingleChoiceID()

    room:placeEntityAt(2, 3, itemType1):setAttribute("item", "singleChoice", singleChoiceGroup)
    room:placeEntityAt(3, 3, itemType2):setAttribute("item", "singleChoice", singleChoiceGroup)
    room:placeEntityAt(4, 3, itemType3):setAttribute("item", "singleChoice", singleChoiceGroup)
end

--- @param room LibLevelGen.Room
local function createBloodShop(room)
    local instance = room.instance
    room:placeEntityAt(3, 2, "Shopkeeper", 2)

    -- TODO logic for 2 items if weaponless char is active
    local levelBoost = math.max(3 - instance:getFloor(), 0)  

    local itemType1 = itemGeneration.choice {
        itemPool = itemGeneration.Pool.CHEST,
        slot = itemSlot.Type.WEAPON,
        requiredComponents = {"weaponMaterialGold"},
        levelBonus = levelBoost
    }
    local itemType2 = itemGeneration.choice {
        itemPool = itemGeneration.Pool.CHEST,
        levelBonus = levelBoost
    }
    local itemType3 = itemGeneration.choice {
        itemPool = itemGeneration.Pool.CHEST,
        levelBonus = levelBoost
    }

    room:placeEntityAt(2, 4, itemType1):setPrice("blood")
    room:placeEntityAt(3, 4, itemType2):setPrice("blood")
    room:placeEntityAt(4, 4, itemType3):setPrice("blood")
end

--- @param room LibLevelGen.Room
local function createGlassShop(room)
    local instance = room.instance
    room:placeEntityAt(3, 2, "Shopkeeper", 3)

    -- TODO logic for 2 items if weaponless char is active

    local glassItemPriotity = {
        ArmorGlass = 0,
        ShovelGlass = 1,
        TorchGlass = 2,
        FeetGlassSlippers = 3
    }
    local glassItems = {"ArmorGlass", "ShovelGlass", "TorchGlass", "FeetGlassSlippers"}
    utils.sort(glassItems, function (a, b)
        local seenA = itemGeneration.getSeenCount(a)
        local seenB = itemGeneration.getSeenCount(b)

        if seenA == seenB then
            return glassItemPriotity[a] < glassItemPriotity[b]
        end
        return seenA < seenB
    end)

    local itemType1 = glassItems[1]
    local itemType2 = glassItems[2]
    local itemType3 = itemGeneration.choice {
        itemPool = itemGeneration.Pool.CHEST,
        slot = itemSlot.Type.WEAPON,
        requiredComponents = {"weaponMaterialGlass"}
    }
    itemGeneration.markSeen(itemType1)
    itemGeneration.markSeen(itemType2)

    room:placeEntityAt(2, 4, itemType1):setPrice():setDiscount(0.5)
    room:placeEntityAt(3, 4, itemType2):setPrice():setDiscount(0.5)
    room:placeEntityAt(4, 4, itemType3):setPrice():setDiscount(0.5)
end

--- @param room LibLevelGen.Room
local function createFoodShop(room)
    local instance = room.instance

    room:placeEntityAt(4, 3, "Shopkeeper", 4)

    local itemType1 = itemGeneration.choice {itemPool = itemGeneration.Pool.FOOD}

    local heartContainerRoll = instance:randIntRange(0, 101)
    local itemType2
    if heartContainerRoll <= 20 then
        itemType2 = "MiscHeartContainer"
    elseif heartContainerRoll <= 25 then
        itemType2 = "MiscHeartContainer2"
    elseif heartContainerRoll <= 50 then
        itemType2 = "MiscHeartContainerEmpty2"
    else
        itemType2 = "MiscHeartContainerEmpty"
    end

    room:placeEntityAt(3, 5, itemType1):setPrice()
    room:placeEntityAt(5, 5, itemType2):setPrice()
end

local function createConjurer(room)
    -- that was easy
    room:placeEntityAt(2, 1, "Conjurer")
end

local function createShriner(room)
    local instance = room.instance

    room:placeEntityAt(3, 3, "Shriner")

    local elligibleShrines = instance:getElligibleShrines()
    utils.removeIf(elligibleShrines, function(shrineType)
        local shrineData = shrine.getData(shrineType)
        return shrineData.notAllowedInShriner == false
    end)

    while #elligibleShrines < 3 do
        -- dbg("filling shriner with extra glass shrine")
        elligibleShrines[#elligibleShrines+1] = shrine.Type.GLASS
    end

    local shrinesToPlace = instance:randChoiceMany(elligibleShrines, 3)
    -- prices are set automatically
    room:placeEntityAt(2, 5, shrine.getData(shrinesToPlace[1]).name)
    room:placeEntityAt(3, 5, shrine.getData(shrinesToPlace[2]).name)
    room:placeEntityAt(4, 5, shrine.getData(shrinesToPlace[3]).name)
end

local function createPawnbroker(room)
    -- cool
    room:placeEntityAt(3, 1, "Pawnbroker")
end

--- @class LibLevelGen.SecretShop
--- @field name string # friendlyName for logging/error message purposes
--- @field w number # (default: 7) - width of the room
--- @field h number # (default: 10) - height of the room
--- @field runeX number # (default: 3) - x position of the travel rune
--- @field runeY number # (default: 7) - y position of the travel rune
--- @field canHaveSSS boolean # (default: false) - determines whether a super secret shop can be created above the secret shop
--- @field tileType string # (default: StoneWallCracked) - wall where the rune spawns will be converted to this type
--- @field convertibleTiles string[] # (default: {"DirtWall", "StoneWall", "CatacombWall"}) - tiles where the rune can be placed. Will be converted to tile specified by tileType.
--- @field runeType string # entity type of the shop travel rune
--- @field clb fun(room: LibLevelGen.Room) # callback function that gets called with the room as the parameter when the shop is spawned
--- @field levelRange number[] # range of level numbers on which the shop is allowed to spawn, in format {minLvl, maxLvl} (inclusive). If not specified, there are no restrictions (apart from not being able to spawn on boss levels)
--- @field SSScurrency string

--- @param args LibLevelGen.SecretShop
--- @return table
local function createSecretShopData(args)
    if args.canHaveSSS == nil then
        args.canHaveSSS = false
    end
    args.SSScurrency = args.SSScurrency or "coins"
    args.w = args.w or 7
    args.h = args.h or 9
    args.runeX = args.runeX or 3
    args.runeY = args.runeY or 6
    args.tileType = args.tileType or "StoneWallCracked"
    args.convertibleTiles = args.convertibleTiles or {"DirtWall", "StoneWall", "CatacombWall"}
    args.name = args.name or "(unnamed)"
    if type(args.runeType) ~= "string" then
        error("args.runeType must be a string")
    end
    if type(args.clb) ~= "function" then
        error("args.clb must be a function")
    end
    return enum.data(args)
end

secretShop.Type = enum.sequence {
    TRANSMOGRIFIER = createSecretShopData{
        tileType = "StoneWallCracked",
        name = "Transmog",
        runeType = "TravelRune1",
        clb = createTransmogrifier
    },
    ARENA = createSecretShopData{
        tileType = "DirtWallCracked",
        name = "Arena",
        runeType = "TravelRune2",
        h = 12,
        clb = createArena
    },
    BLOOD_SHOP = createSecretShopData{
        tileType = "StoneWallCracked",
        name = "Blood shop",
        runeType = "TravelRune3",
        canHaveSSS = true,
        SSScurrency = "health",
        levelRange = {1, 2},
        clb = createBloodShop
    },
    GLASS_SHOP = createSecretShopData{
        tileType = "StoneWallCracked",
        name = "Glass shop",
        runeType = "TravelRune4",
        canHaveSSS = true,
        clb = createGlassShop
    },
    FOOD_SHOP = createSecretShopData{
        tileType = "CatacombWallCracked",
        name = "Food shop",
        runeType = "TravelRune5",
        w = 9,
        h = 11,
        runeX = 4,
        runeY = 7,
        canHaveSSS = true,
        clb = createFoodShop
    },
    CONJURER = createSecretShopData{
        tileType = "DirtWallCracked",
        name = "Conjurer",
        runeType = "TravelRune6",
        h = 10,
        runeY = 7,
        clb = createConjurer
    },
    SHRINER = createSecretShopData{
        tileType = "DirtWallCracked",
        name = "Shriner",
        runeType = "TravelRune7",
        h = 10,
        runeY = 7,
        clb = createShriner
    },
    PAWNBROKER = createSecretShopData{
        tileType = "CatacombWallCracked",
        name = "Pawnbroker",
        runeType = "TravelRune8",
        h = 10,
        runeY = 7,
        clb = createPawnbroker
    }
}

local function valuesWithinRange(values, range)
    local res = {}
    for i = 1, #values do
        local value = values[i]
        if value >= range[1] and value <= range[2] then
            res[#res+1] = value
        end
    end
    return res
end

local function findAndRemoveValue(values, value)
    for i = 1, #values do
        if values[i] == value then
            table.remove(values, i)
            return
        end
    end
end

--- @param generatorData LibLevelGen.Config\
--- @param rng LibLevelGen.Rng
--- @return table<number,number>
function secretShop.createSecretShopLevelNumbers(generatorData, rng)
    local res = {}
    if generatorData.placeSecretShops then

        local maxLevel = generatorData.maxLevel
        local elligibleFloorNumbers = {}
        for i = 1, maxLevel do
            local level = levelSequence.get(i)
            ---@diagnostic disable-next-line: undefined-field
            if level and not level.boss then
                elligibleFloorNumbers[#elligibleFloorNumbers+1] = i
            end
        end

        local placementPriorityValues = {}
        local placementPriority = {}
        for _, enumValue in pairs(secretShop.Type) do
            local data = secretShop.getData(enumValue)
            local range = data.levelRange or {1, generatorData.maxLevel}
            local numberOfAllowedLevels = range[2] - range[1] + 1
            if not placementPriority[numberOfAllowedLevels] then
                placementPriorityValues[#placementPriorityValues+1] = numberOfAllowedLevels
                placementPriority[numberOfAllowedLevels] = {enumValue}
            else
                local tmp = placementPriority[numberOfAllowedLevels]
                tmp[#tmp+1] = enumValue
            end
        end
        table.sort(placementPriorityValues)

        for i = 1, #placementPriorityValues do
            local priorityValue = placementPriorityValues[i]
            local shopsWithThisPriority = placementPriority[priorityValue]
            for j = 1, #shopsWithThisPriority do
                local enumValue = shopsWithThisPriority[j]

                local shopData = secretShop.getData(enumValue)
                local range = shopData.levelRange or {1, generatorData.maxLevel}

                local levelsThisShopCanBePlacedOn = valuesWithinRange(elligibleFloorNumbers, range)
                if #levelsThisShopCanBePlacedOn == 0 then
                    if not generatorData.allowSecretShopPlacementFailures then
                        error(
                            "cannot find where to place secret shop " .. shopData.name .. ": no elligible levels available\n" ..
                            "\n(use LibLevelGen.Config.allowSecretShopPlacementFailures to suppress this error)\n"
                        )
                    end
                else
                    local levelNumber = rng:randChoice(levelsThisShopCanBePlacedOn)
                    findAndRemoveValue(elligibleFloorNumbers, levelNumber)

                    res[levelNumber] = enumValue
                end
            end
        end
    end

    --dbg("Secret shop level numbers", res)

    return res
end

--- @param name string
--- @param args LibLevelGen.SecretShop
--- @return number
function secretShop.register(name, args)
    args.name = name
    return secretShop.Type.extend(args.name, createSecretShopData(args))
end

--- @param shopType number # From the secretShop.Type enum.
--- @return LibLevelGen.SecretShop
function secretShop.getData(shopType)
    return secretShop.Type.data[shopType]
end

return secretShop
