local levelSequence = require "LibLevelGen.LevelSequence"
local gameDLC = require "necro.game.data.resource.GameDLC"
local enum = require "system.utils.Enum"
local event = require "necro.event.Event"
local object = require "necro.game.object.Object"
local tile = require "necro.game.tile.Tile"
local levelGenerator = require "necro.game.level.LevelGenerator"
local gameSession = require "necro.client.GameSession"
local itemGeneration = require "necro.game.item.ItemGeneration"

local ecs = require "system.game.Entities"
local util = require "system.utils.Utilities"
local try = require "system.utils.Try"

local errorLevel = require "LibLevelGen.ErrorLevel"
local room = require "LibLevelGen.Room"
local segment = require "LibLevelGen.Segment"
local settings = require "LibLevelGen.Settings"
local secretShop = require "LibLevelGen.SecretShop"
local shrine = require "LibLevelGen.Shrine"
local rand = require "LibLevelGen.Rand"
local levelGenUtil = require "LibLevelGen.Util"
local loot = require "LibLevelGen.Loot"

local libLevelGen = {}

--- @class LibLevelGen.RunState
--- @field libLevelGenDataInitialized boolean # Whether the runState has been initialized with libLevelGen-specific fields. Used when transitioning from vanilla runState format.
--- @field previousLevelMinibosses table # List of minibosses from the previous level.
--- @field secretShopLevels table<number,number> # Level number to shop ID mapping of secret shop levels.
--- @field seenItems table<string,number> # Mapping of seen item counts.
--- @field seenShrines number[] # Seen shrine IDs
--- @field shrineLevels table<number,number> # Level number to shrine ID mapping of levels where shrines should spawn.
--- @field shopkeeperDead boolean # Whether the shopkeeper has been killed.
--- @field shopkeeperGhostLevel number # What level the shopkeeper ghost should spawn in (relative to start of the zone)
--- @field shopkeeperGhostDepth number # What depth the shopkeeper ghost should spawn in
--- @field peaceRingActive boolean # Whether the ring of peace is active
--- @field warRingActive boolean # Whether the ring of war is active
--- @field warShrineActive boolean # Whether the shrine of war is active
--- @field bossShrineActive boolean # Whether the boss shrine is active
--- @field spaceShrineActive boolean # Whether the space shrine is active
--- @field lockedShopPlaced boolean # Whether a locked shop has been placed in this run
--- @field secretRockRoomPlaced boolean # Whether a vault has been placed in this run
--- @field urnPlaced boolean # Whether an urn has been placed in this run
--- RunState used by LibLevelGen levels, compatible with the vanilla runState format.

--- @class LibLevelGen.Config
--- @field allowLevel1Shrine boolean # Whether a shrine is allowed to spawn on 1-1 or not
--- @field allowOldShrines boolean # Whether shrines that have been disabled in the AMPLIFIED DLC can spawn
--- @field disableEnemyUpgrades boolean # Whether enemy upgrades (or downgrades) through ring of war/peace/shrines etc should be disabled
--- @field zones number # Amount of zones
--- @field levelsPerZone number # Amount of levels per zone
--- @field maxLevel number # Max level number
--- @field placeSecretShops boolean # Whether secret shops should be automatically placed
--- @field placeShrines boolean # Whether shrines should be automatically placed
--- @field allowSecretShopPlacementFailures boolean # Whether errors related to secret shop generation should be suppressed.
--- @field sequenceClb fun(ev:Event.LevelSequenceUpdate, cfg: LibLevelGen.Config) # Function which will be used to generate level sequence data.
--- Configuration that enables or disables various built-in features of libLevelGen.

--- @class LibLevelGen.Generator
--- @field name string # Name of the generator
--- @field func function # Callback function of the generator
--- @field data LibLevelGen.Config # Configuration of the generator
--- LibLevelGen generation data registered by `LibLevelGen.registerGenerator`.

--- @class LibLevelGen.LevelGenerationOptions : LevelGenerator.Options
--- @field zone number # Current zone number
--- @field depth number # Current depth number
--- @field floor number # Current floor number
--- Configuration of the currently generated level

--- @class LibLevelGen.LevelGenerationEventParameters
--- @field callback function # The function that should be called with the generated level data as the argument
--- @field data LibLevelGen.LevelGenerationOptions # Configuration of the current level
--- @field failMaps number # Amount of times the generator has failed to generate this level
--- @field libLevelGenGeneratorData LibLevelGen.Config # Configuration of the level generator
--- @field runState LibLevelGen.RunState # State of the current run
--- Data passed by level generation event

--- @class LibLevelGen.MusicData
--- @field id string # ID of the music set. Generally you want to set it to `"OST"`, unless you're registering custom music.
--- @field type string # For `id = "OST"`, it's either `"zone"` or `"boss"`.
--- @field level number # Level number of the music
--- @field zone number # Zone number of the music
--- Structure that specifies music for the level

local mt = {
    --- @class LibLegelGen.Instance
    --- @field rng LibLevelGen.Rng # RNG instance used by this generator instance
    --- @field levelGeneratorData LibLevelGen.Config # Configuration of the generator used
    --- @field levelGenerationOptions LibLevelGen.LevelGenerationOptions # Level generation options for the current level
    --- @field runState LibLevelGen.RunState # RunState
    --- @field segments LibLevelGen.Segment[] # Array of segments in this instance
    --- @field seed number # Seed of the entire run
    --- @field isFinal boolean # Whether the current level is the last one
    --- @field currentSeed number # Seed of this level
    --- @field singleChoiceID number
    --- @field level number # Current level number
    --- @field zone number # Current zone
    --- @field depth number # Current depth
    --- @field floor number # Current floor
    --- @field music LibLevelGen.MusicData # Music data for the generated level
    --- @field tileset string # Tileset name that's inherited by newly created segments
    --- @field levelsPerZone number # Amount of levels per zone
    --- @field boss number # Whether the current level is a boss level. If it's not, boss is set to 0; Otherwise it's the boss ID.
    --- @field callback function # Callback to call with the generated level data
    --- Instance of a level generator, used to do basically everything as far as generating levels goes.
    __index = {
        segments = {},
        seed = 1,
        currentSeed = 1,
        level = 1,
        isFinal = false,
        runState = {},
        music = {
            id = "OST",
            level = 1,
            type = "zone",
            zone = 1
        },
        tileset = "Zone1",

        --- !!! Internal
        --- @param self LibLegelGen.Instance
        --- @param ev LibLevelGen.LevelGenerationEventParameters
        init = function(self, ev)
            -- dbg(ev)
            self.segments = {}
            self.rng = rand.new(ev.data.seed + ev.failMaps)
            self.seed = ev.data.seed
            self.isFinal = ev.data.isFinal
            self.level = ev.data.number
            self.zone = ev.data.zone
            self.depth = ev.data.depth
            self.floor = ev.data.floor
            self.levelsPerZone = ev.libLevelGenGeneratorData.levelsPerZone
            self.boss = 0
            self.runState = ev.runState or {}
            self.callback = ev.callback
            self.currentSeed = self.seed
            self.singleChoiceID = 1
            self:advancePerLevelRNG()
            self.currentSeed = self.currentSeed + ev.failMaps
            self.levelGeneratorData = ev.libLevelGenGeneratorData
            self.levelGenerationOptions = ev.data
            self.runState.seenItems = self.runState.seenItems or {}
            self:setMusic {
                id = "OST",
                level = math.min(self:getFloor(), 3),
                type = "zone",
                zone = math.min(self:getDepth(), 5)
            }
        end,

        --- !!! Internal
        --- @param self LibLegelGen.Instance
        --- @return number
        getNewSingleChoiceID = function(self)
            local res = self.singleChoiceID
            self.singleChoiceID = res + 1
            return res
        end,

        --- Get current depth
        --- @param self LibLegelGen.Instance # The instance
        --- @return number
        getDepth = function(self)
            return self.depth
        end,

        --- Get current floor
        --- @param self LibLegelGen.Instance # The instance
        --- @return number
        getFloor = function(self)
            return self.floor
        end,

        --- Set music for this level
        --- @param self LibLegelGen.Instance # the instance
        --- @param music LibLevelGen.MusicData # Music data
        setMusic = function(self, music)
            self.music = music
        end,

        --- Set default tileset
        --- @param self LibLegelGen.Instance # The instance
        --- @param tilesetName string # Tileset name
        setTileset = function (self, tilesetName)
            self.tileset = tilesetName
        end,

        --- Generate a random 32-bit integer number.
        --- @param self LibLegelGen.Instance # The instance
        --- @return number
        rand = function(self)
            return self.rng:rand()
        end,

        --- Generate a random float within the given range.
        --- @param self LibLegelGen.Instance # The instance
        --- @param min number # Minimum value
        --- @param max number # Maximum value
        --- @return number
        randFloatRange = function (self, min, max)
            return self.rng:randFloatRange(min, max)
        end,

        --- Generate a random integer within the given range.
        --- @param self LibLegelGen.Instance # The instance
        --- @param min number # Minimum value (inclusive)
        --- @param max number # Maximum value (not inclusive)
        --- @return number
        randIntRange = function(self, min, max)
            return self.rng:randIntRange(min, max)
        end,

        --- Choose a random element from the given table.
        --- @generic T
        --- @param self LibLegelGen.Instance # The instance
        --- @param t T[] # Table to choose an element from
        --- @param remove? boolean # Whether the chosen element should be removed from the table, defaults to false
        --- @return T
        randChoice = function(self, t, remove)
            return self.rng:randChoice(t, remove)
        end,

        --- Choose n elements from the given table.
        --- @generic T
        --- @param self LibLegelGen.Instance # The instance
        --- @param choices T[] # Table to choose elements from
        --- @param n number # Number of elements to choose
        --- @param remove? boolean # Whether the chosen elements should be removed from the table, defaults to false
        --- @return T
        randChoiceMany = function(self, choices, n, remove)
            return self.rng:randChoiceMany(choices, n, remove)
        end,

        --- Randomly returns boolean based on given probability.
        --- @param self LibLegelGen.Instance # The instance
        --- @param probability number # Value between 0.0 and 1.0
        --- @return boolean
        randChance = function(self, probability)
            return self.rng:randChance(probability)
        end,

        --- Randomly choose a value from the weighted choice data.
        --- @param self LibLegelGen.Instance # The instance
        --- @param chances table<string,number> # Mapping of values that can be chosen to their probability (does not have to add up to 1)
        --- @return string
        randWeightedChoice = function(self, chances)
            return self.rng:randWeightedChoice(chances)
        end,

        --- !!! Internal
        --- @param self LibLegelGen.Instance
        advancePerLevelRNG = function(self)
            for i = 1, self.level do
                self.rng:rand()
            end
        end,

        --- Creates a new segment.
        --- @param self LibLegelGen.Instance # The instance
        --- @return LibLevelGen.Segment
        createSegment = function(self)
            local newSegment = segment.new(self)
            newSegment:setTileset(self.tileset)
            self.segments[#self.segments+1] = newSegment
            return newSegment
        end,

        --- !!! Internal
        --- @param self LibLegelGen.Instance
        --- @param tileMapping table
        --- @return table, table
        serializeSegments = function(self, tileMapping)
            local res = {}
            local allSegmentOffsets = {}
            local previousSegmentBounds = nil
            for i = 1, #self.segments do
                local serialized, offset = self.segments[i]:serialize(tileMapping, previousSegmentBounds)
                if serialized then
                    previousSegmentBounds = serialized.bounds
                    allSegmentOffsets[#allSegmentOffsets+1] = offset
                    res[#res+1] = serialized
                else
                    error("segment serialization failed and I don't know why")
                end
            end
            return res, allSegmentOffsets
        end,

        --- !!! Internal
        --- @param self LibLegelGen.Instance
        --- @return table
        generateAreas = function(self)
            local res = {}
            for i = 1, #self.segments do
                local segment = self.segments[i]
                for j = 1, #segment.rooms do
                    res[#res+1] = segment.rooms[j]:getBounds()
                end
            end
            return res
        end,

        --- !!! Internal
        --- @param self LibLegelGen.Instance
        --- @return table
        generateTileMapping = function(self)
            local mapping = {
                Zone1 = {
                    id = 0,
                    LevelBorder = 0,
                    Void = 1
                }
            }

            local tileNames = {
                "LevelBorder",
                "Void",
            }
            local tilesetNames = {
                [0] = "Zone1",
            }
            local tilesets = {
                0,
                0
            }

            local tileIndex = 3
            local tilesetIndex = 1

            local function addTileToMappingIfNeeded(tile)
                if not mapping[tile.tileset] then
                    mapping[tile.tileset] = {id = tilesetIndex}
                    tilesetNames[tilesetIndex] = tile.tileset
                    tilesetIndex = tilesetIndex + 1
                end
                local tilesetMapping = mapping[tile.tileset]
                if not tilesetMapping[tile.type] then
                    tilesetMapping[tile.type] = tileIndex
                    tileNames[#tileNames+1] = tile.type
                    tileIndex = tileIndex + 1
                    tilesets[#tilesets+1] = tilesetMapping.id
                end
            end

            for i = 1, #self.segments do
                local segment = self.segments[i]

                if segment.padding.mainTile then
                    addTileToMappingIfNeeded(segment.padding.mainTile)
                end
                if segment.padding.extraTiles then
                    for _, tile in ipairs(segment.padding.extraTiles) do
                        addTileToMappingIfNeeded(tile)
                    end
                end

                for j = 1, #segment.rooms do
                    local room = segment.rooms[j]
                    for k = 1, #room.tiles do
                        local tile = room.tiles[k]
                        addTileToMappingIfNeeded(tile)
                    end
                end
            end
            return mapping, tileNames, tilesetNames, tilesets
        end,

        --- !!! Internal
        --- @param self LibLegelGen.Instance
        --- @param allSegmentOffsets table
        serializeEntities = function(self, allSegmentOffsets)
            local res = {}
            for i = 1, #self.segments do
                local segment = self.segments[i]
                local segmentOffset = allSegmentOffsets[i]
                for j = 1, #segment.rooms do
                    local room = segment.rooms[j]
                    for k = 1, #room.tiles do
                        local tile = room.tiles[k]
                        for l = 1, #tile.entities do
                            local entityCopy = tile.entities[l]:serialize()
                            entityCopy.x = entityCopy.x + room.x + segmentOffset[1]
                            entityCopy.y = entityCopy.y + room.y + segmentOffset[2]
                            res[#res+1] = entityCopy
                        end
                    end
                end
            end
            return res
        end,

        --- !!! Internal
        --- @param self LibLegelGen.Instance
        placeSecretShopIfNeeded = function(self)
            local secretShopLevels = self.runState.secretShopLevels
            local shopToPlaceOnThisLevel = secretShopLevels[self.level]
            if shopToPlaceOnThisLevel then
                local shopData = secretShop.getData(shopToPlaceOnThisLevel)

                -- Find wall where travel rune can be placed (main segment only)
                --- @type LibLevelGen.Tile[]
                local elligibleTiles = {}
                local mainSegment = self.segments[1]
                for j = 1, #shopData.convertibleTiles do
                    local convertibleTileType = shopData.convertibleTiles[j]
                    for i = 1, #mainSegment.rooms do
                        local currentRoom = mainSegment.rooms[i]
                        if currentRoom:checkFlags(room.Flag.ALLOW_TRAVELRUNE) then
                            local elligibleTilesInThisRoom = currentRoom:selectTiles{type = convertibleTileType, nearFloor = true}
                            util.appendToTable(elligibleTiles, elligibleTilesInThisRoom)
                        end
                    end
                end

                if #elligibleTiles == 0 then
                    if self.levelGeneratorData.allowSecretShopPlacementFailures then
                        return
                    end
                    -- I hope no one actually runs into this
                    error(
                        "no elligible tiles to place secret shop " .. shopData.name .. "\n" ..
                        "\n(use LibLevelGen.Config.allowSecretShopPlacementFailures to suppress this error)\n"
                    )
                end

                local selectedTile = self:randChoice(elligibleTiles)
                local selectedRoom = selectedTile.room
                -- selectedRoom:placeEntity(selectedTile, shopData.runeType):setAttribute("visibility", "visible", false)
                selectedTile:convert(shopData.tileType)

                local secretShopSegment = self:createSegment()
                local secretShopRoom = secretShopSegment:createRoom(0, 0, shopData.w, shopData.h, room.Type.SECRET_SHOP)
                secretShopRoom:fill("Floor")
                secretShopRoom:placeEntityAt(shopData.runeX, shopData.runeY, shopData.runeType)

                if shopData.canHaveSSS and self:randChance(0.1) then
                    secretShopRoom:border {
                        rect = {
                            x = 0,
                            y = 1,
                            w = shopData.w,
                            h = shopData.h - 1
                        },
                        sides = {
                            top = 1
                        },
                        tileType = "ShopWall"
                    }
                    secretShopRoom:border {
                        sides = {
                            left = 1,
                            right = 1,
                            bottom = 1
                        },
                        tileType = "ShopWall"
                    }
                    local superSecretShopRoom = secretShopSegment:createRoom(0, -7, shopData.w, 7, room.Type.SECRET_SHOP)
                    superSecretShopRoom:fill("Floor")
                    superSecretShopRoom:border {
                        tileType = "ShopWall",
                        sides = {
                            top = 1,
                            left = 1,
                            right = 1
                        }
                    }
                    superSecretShopRoom:placeWallTorches(8)
                    superSecretShopRoom:placeEntityAt(3, 2, "Medic")

                    local itemType = itemGeneration.choice { itemPool = itemGeneration.Pool.SHOP }
                    local saleItem = superSecretShopRoom:placeEntityAt(3, 4, itemType)
                    if (shopData.SSScurrency == "health") then
                        saleItem:setPrice("health", 1)
                    else
                        saleItem:setPrice(shopData.SSScurrency):setDiscount(0.1)
                    end
                else
                    secretShopRoom:border("ShopWall")
                end

                shopData.clb(secretShopRoom)
                secretShopRoom:placeWallTorches(8) -- TODO: use logic for this
            end
        end,

        --- !!! Internal
        --- @param self LibLegelGen.Instance
        placeShrineIfNeeded = function(self)
            local shrineLevels = self.runState.shrineLevels
            local shrineToPlaceOnThisLevel = shrineLevels[self.level]
            if shrineToPlaceOnThisLevel then
                local mainSegment = self.segments[1]
                local elligibleShrineRooms = mainSegment:selectRooms(room.Flag.ALLOW_SHRINE)
                if #elligibleShrineRooms == 0 then
                    error("no rooms elligible to place shrine")
                end
                local shrineData = shrine.getData(shrineToPlaceOnThisLevel)
                local shrineRoom = self:randChoice(elligibleShrineRooms)
                shrineRoom:placeEntity(shrineRoom:chooseRandomTile(levelGenUtil.TileRequirements.Shrine.Generic), shrineData.name)
                -- shrine is marked seen when shrineLevels are determined
            end
        end,

        --- !!! Internal
        --- @param self LibLegelGen.Instance
        getElligibleShrines = function(self)
            return shrine.getElligibleShrines(self.runState.seenShrines, self.levelGeneratorData)
        end,

        --- Serialize the level and invoke the callback to load it. This should be the last thing your generator calls.
        --- @param self LibLegelGen.Instance # The instance
        finalize = function(self)
            if self.levelGeneratorData.placeSecretShops then
                self:placeSecretShopIfNeeded()
            end
            if self.levelGeneratorData.placeShrines then
                self:placeShrineIfNeeded()
            end
            for _, currentSegment in ipairs(self.segments) do
                loot.placeChests(currentSegment)
                loot.placeCrates(currentSegment)
            end

            local tileMapping, tileNames, tilesetNames, tilesets = self:generateTileMapping()
            local serializedSegments, allSegmentOffsets = self:serializeSegments(tileMapping)
            local res = {
                areas = self:generateAreas(),
                entities = self:serializeEntities(allSegmentOffsets),
                musicZone = self.music.zone,
                musicFloor = self.music.level,
                segments = serializedSegments,
                tileMapping = {
                    tileNames = tileNames,
                    tilesetNames = tilesetNames,
                    tilesets = tilesets,
                },
                zone = self.zone,
                depth = self.depth,
                floor = self.floor,
                num = self.level,
                seed = self.seed,
                boss = self.boss,
                isFinal = self.isFinal,
                runState = self.runState,
            }

            self.callback(res)
        end,
    }
}

--- Creates a new libLevelGen generator instance.
--- @param ev LibLevelGen.LevelGenerationEventParameters # Parameters from the level generation event
--- @return LibLegelGen.Instance
function libLevelGen.new(ev)
    local instance = {}
    setmetatable(instance, mt)
    instance:init(ev)
    return instance
end

--- @param data LibLevelGen.Config
--- @return LibLevelGen.Config
local function fillDefaultData(data)
    if not data then
        data = {}
    end
    data.zones = data.zones or (gameDLC.isAmplifiedLoaded() and 5 or 4)
    data.levelsPerZone = data.levelsPerZone or 4
    data.maxLevel = data.maxLevel or (data.zones * data.levelsPerZone)

    data.placeSecretShops = data.placeSecretShops == true

    data.placeShrines = data.placeShrines == true
    data.allowLevel1Shrine = data.allowLevel1Shrine == true
    data.allowOldShrines = data.allowOldShrines == true

    data.disableEnemyUpgrades = data.disableEnemyUpgrades == true

    data.sequenceClb = data.sequenceClb or levelSequence.templateBasic()

    return data
end

function maybe(val, fallback)
    if val ~= nil then
        return val
    else
        return fallback
    end
end

--- Registers a new generator (it will appear in the extra modes menu). Returns IDs of the generator, regular and seeded game modes created.
--- @param name string # Name of the generator
--- @param func function # The callback function that will be called to generate the level
--- @param data? LibLevelGen.Config # Generator configuration, fields can be left empty to provide default values.
--- @param modeData? GameSession.Mode.Data # Game mode data, fields can be left empty to provide default values.
--- @return number,number,number
function libLevelGen.registerGenerator(name, func, data, modeData)
    -- local generatorType = levelGenerator.Type.extend("libLevelGen_" .. name, enum.data(modeData or {}))--, enum.data{name = name, func = func, data = fillDefaultData(data)})
    local libLevelGenGeneratorData = fillDefaultData(data)
    local generatorType = levelGenerator.Type.extend("libLevelGen_" .. name, enum.data(libLevelGenGeneratorData))

    local opts = modeData or {}
    opts.id = generatorType
    opts.name = name
    opts.generatorOptions = {
        type = generatorType
    }
    opts.diamondCounter = maybe(opts.diamondCounter, false)
    opts.cutscenes = maybe(opts.cutscenes, false)
    opts.depthPriceMultiplier = maybe(opts.depthPriceMultiplier, true)

    local modeID = gameSession.registerMode(opts)
    local seededOpts = util.shallowCopy(opts)
    seededOpts.id = seededOpts.id .. "_seeded"
    seededOpts.name = seededOpts.name .. " (seeded)"
    seededOpts.seedHUD = true
    seededOpts.seedMode = gameSession.SeedMode.MANUAL
    local seededModeID = gameSession.registerMode(seededOpts)

    event.levelGenerate.add(nil, generatorType, function (ev)
        --- @type LibLevelGen.LevelGenerationEventParameters
        local params = {
            libLevelGenGeneratorData = libLevelGenGeneratorData,
            data = ev.options,
            failMaps = 0,
            runState = ev.options.runState,
            callback = function (serializedLevelData)
                ev.level = serializedLevelData
            end
        }
        libLevelGen.initLibLevelGenRunState(params.runState, params)

        if settings.retryOnFailmap() then
            while params.failMaps < 100 do
                local res, err = pcall(func, params)
                if res then
                    break
                end
                if settings.enableErrorLogging() then
                    log.error(err)
                end
                params.failMaps = params.failMaps + 1
            end
            if params.failMaps >= 100 then
                error("Failmap limit exceeded")
                log.error("Failmap limit exceeded")
            end
        else
            func(params)
        end
    end)

    return generatorType, modeID, seededModeID
end

--- @type function[]
local postProceessors = {}

-- --- @param func function
-- function libLevelGen.registerPostProcessor(func)
--     postProceessors[#postProceessors+1] = func
-- end

--- !!! Internal
--- @param runState LibLevelGen.RunState
--- @param ev LibLevelGen.LevelGenerationEventParameters
function libLevelGen.initLibLevelGenRunState(runState, ev)
    if not runState.libLevelGenDataInitialized then
        runState.libLevelGenDataInitialized = true
        local rng = rand.new(ev.data.seed)
        runState.secretShopLevels = secretShop.createSecretShopLevelNumbers(ev.libLevelGenGeneratorData, rng)
        runState.seenShrines = runState.seenShrines or {}
        runState.shrineLevels = shrine.createShrineLevelNumbers(runState.seenShrines, ev.libLevelGenGeneratorData, rng)
        runState.previousLevelMinibosses = {}
    end
end


event.levelSequenceUpdate.add("libLevelGen", {order = "initCustomDungeon", sequence = 1},
--- @param ev Event.LevelSequenceUpdate
function (ev)
    --- @type LibLevelGen.Config
    local config = levelGenerator.Type.data[ev.options.type]
    if config and config.sequenceClb then
        config.sequenceClb(ev, config)

        -- Fill required data
        for i=1, #ev.sequence do
            local entry = ev.sequence[i]
            local prev = ev.sequence[i - 1] or {zone = 1, depth = 1, floor = 0}
            entry.zone = entry.zone or prev.zone
            entry.depth = entry.depth or prev.depth
            entry.floor = entry.floor or (prev.floor + 1)
        end
    end
end)

return libLevelGen
