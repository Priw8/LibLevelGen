local collision = require "necro.game.tile.Collision"
local util = require "system.utils.Utilities"

local room = require "LibLevelGen.Room"

local levelGenUtil = require "LibLevelGen.Util"

local TileRequirements = levelGenUtil.TileRequirements

local corridorWallsForAxis = {
    [room.Axis.VERTICAL] = {left = 1, right = 1},
    [room.Axis.HORIZONTAL] = {top = 1, bottom = 1}
}

local segment = {}

-- TODO: make these classes not declared all over the place

---@class LibLevelGen.RoomGenCombination
---@field direction number
---@field corridorEntrance number
---@field corridorExit number
---@field corridorThickness number
---@field corridorLength number
---@field roomWidth number
---@field roomHeight number
---@field clearCorridor boolean
---@field entranceTileChances table<string,number>
---@field exitTileChances table<string,number>

---@class LibLevelGen.RoomGenCombinations
---@field direction number[]
---@field corridorEntrance number[]
---@field corridorExit number[]
---@field corridorThickness number[]
---@field corridorLength number[]
---@field roomWidth number[]
---@field roomHeight number[]
---@field clearCorridor boolean
---@field entranceTileChances table<string,number>
---@field exitTileChances table<string,number>

---@class LibLegelGen.RoomGenResult
---@field axis number
---@field corridorX number
---@field corridorY number
---@field roomCorridorX number
---@field roomCorridorY number
---@field combination LibLevelGen.RoomGenCombination

--- @class LibLevelGen.TileData
--- @field tileset string
--- @field type string

--- @class LibLevelGen.PaddingExtraTileData : LibLevelGen.TileData
--- @field ratio number

--- @class LibLevelGen.PaddingData
--- @field mainTile LibLevelGen.TileData
--- @field extraTiles LibLevelGen.PaddingExtraTileData[]
--- @field count number

--- @class LibLevelGen.Rect
--- @field x number
--- @field y number
--- @field w number
--- @field h number

local mt = {
    --- @class LibLevelGen.Segment
    --- @field instance LibLegelGen.Instance The instance that created this segment.
    --- @field rooms LibLevelGen.Room[] Rooms within this segment.
    --- @field padding table Wall padding that's applied to rooms when serializing the segment.
    --- @field tileset string The default tileset that's used if not specified
    --- @field roomBorder string Tile type that's used for walls in newly created rooms.
    --- @field corridorBorder string Tile type that's used for walls in newly created corridors.
    --- @field chestsLeft number chests that have yet to be placed.
    --- @field cratesLeft number of crates that have yet to be placed.
    --- @field forceSecretRoomBomb boolean Determines whether 1st secret room will be forcefully filled with a bomb (used when locked shop spawns)
    __index = {
        rooms = {},
        padding = {
            count = 0
        },
        tileset = "Zone1",
        roomBorder = "DirtWall",
        corridorBorder = "DirtWall",
        chestsLeft = 0,
        cratesLeft = 0,
        init = function(self)
            self.rooms = {}
        end,

        -- Sets the amount of chests that should be placed in this segment.
        --- @param self LibLevelGen.Segment
        --- @param chests number
        setNumberOfChests = function(self, chests)
            self.chestsLeft = chests
        end,

        -- Sets the amount of crates that should be placed in this segment.
        --- @param self LibLevelGen.Segment
        --- @param crates number
        setNumberOfCrates = function(self, crates)
            self.cratesLeft = crates
        end,

        -- Sets the default tileset.
        --- @param self LibLevelGen.Segment
        --- @param tilesetName string
        setTileset = function(self, tilesetName)
            self.tileset = tilesetName
        end,

        -- Sets the room border that's used in newly created rooms.
        --- @param self LibLevelGen.Segment
        --- @param roomBorder string
        setRoomBorder = function(self, roomBorder)
            self.roomBorder = roomBorder
        end,

        -- Sets the corridor border that's used in newly created corridors.
        --- @param self LibLevelGen.Segment
        --- @param corridorBorder string
        setCorridorBorder = function(self, corridorBorder)
            self.corridorBorder = corridorBorder
        end,

        -- Sets the padding that's applied to rooms when serializing the segment.
        --- @param self LibLevelGen.Segment
        --- @param data LibLevelGen.PaddingData
        setPadding = function(self, data)
            self.padding = data
        end,

        -- Returns a padding tile from the given padding data.
        --- @param self LibLevelGen.Segment
        --- @param padding LibLevelGen.PaddingData
        --- @return LibLevelGen.TileData
        getRandomPaddingTile = function(self, padding)
            for i = 1, #padding.extraTiles do
                local tile = padding.extraTiles[i]
                if self.instance:randChance(tile.ratio) then
                    return tile
                end
            end
            return padding.mainTile
        end,

        -- Creates a new room at specified coordinates.
        --- @param self LibLevelGen.Segment
        --- @param x number
        --- @param y number
        --- @param w number
        --- @param h number
        --- @param flags number?
        --- @return LibLevelGen.Room
        createRoom = function(self, x, y, w, h, flags)
            local newRoom = room.new(self.instance, self, x, y, w, h, flags)
            self.rooms[#self.rooms + 1] = newRoom
            return newRoom
        end,

        --- Creates a starting room.
        --- @param self LibLevelGen.Segment
        --- @return LibLevelGen.Room
        createStartingRoom = function(self)
            return self:createRoom(-3, -3, 7, 7, room.Type.STARTING)
        end,

        --- Places specified amount of torches in rooms that allow it.
        --- @param self LibLevelGen.Segment
        --- @param torchesPerRoom number
        placeWallTorches = function(self, torchesPerRoom)
            for i = 1, #self.rooms do
                local currentRoom = self.rooms[i]
                if currentRoom:checkFlags(room.Flag.ALLOW_TORCH) then
                    currentRoom:placeWallTorches(torchesPerRoom)
                end
            end
        end,

        --- Select rooms that match the given flags.
        --- @param self LibLevelGen.Segment
        --- @param flags number
        --- @return LibLevelGen.Room[]
        selectRooms = function(self, flags)
            local res = {}
            for i = 1, #self.rooms do
                local currentRoom = self.rooms[i]
                if currentRoom:checkFlags(flags) then
                    res[#res+1] = currentRoom
                end
            end
            return res
        end,

        --- Call the specified callback function for every room that matches the given flags.
        --- @param self LibLevelGen.Segment
        --- @param flags number
        --- @param clb fun(room: LibLevelGen.Room)
        iterateRooms = function(self, flags, clb)
            for i = 1, #self.rooms do
                local currentRoom = self.rooms[i]
                if currentRoom:checkFlags(flags) then
                    clb(currentRoom)
                end
            end
        end,

        --- Randomly convert tiles from sourceType to targetType in rooms that allow tile conversions.
        --- Chance to convert a given tile is specified by ratio.
        --- @param self LibLevelGen.Segment
        --- @param sourceType string
        --- @param targetType string
        --- @param ratio number
        randomlyConvertTiles = function(self, sourceType, targetType, ratio)
            self:iterateRooms(room.Flag.ALLOW_TILE_CONVERSION,
            --- @param currentRoom LibLevelGen.Room
            function (currentRoom)
                currentRoom:randomlyConvertTiles(sourceType, targetType, ratio)
            end)
        end,

        -- This function attempts to generate a linked room with createLinkedRoom,
        -- by using random combinations of given valid values. If it succeeds generating with any of the possible combinations,
        -- the corridor and the new room are returned, as well as table with the room generation data.
        -- If rotateRoom is true, width and height of new room will be swapped if corriddor axis is horizontal
        -- combinations are a return value of segment.createRandLinkedRoomParameterCombinations.
        --- @param self LibLevelGen.Segment
        --- @param roomToLink LibLevelGen.Room
        --- @param rotateRoom boolean
        --- @param orgCombinations LibLevelGen.RoomGenCombination[]
        --- @return LibLevelGen.Room newCorridor, LibLevelGen.Room newRoom, LibLegelGen.RoomGenResult result
        createRandLinkedRoom = function(self, roomToLink, rotateRoom, orgCombinations)
            --- @type LibLevelGen.RoomGenCombination[]
            local combinations = util.deepCopy(orgCombinations)
            while #combinations > 0 do
                local selectedCombination = self.instance:randChoice(combinations, true)
                local axis
                local corridorX, corridorY, corridorW, corridorH
                local roomCorridorX, roomCorridorY, roomW, roomH
                if selectedCombination.direction == room.Direction.LEFT then
                    axis = room.Axis.HORIZONTAL
                    roomW = rotateRoom and selectedCombination.roomHeight or selectedCombination.roomWidth
                    roomH = rotateRoom and selectedCombination.roomWidth or selectedCombination.roomHeight
                    corridorX = roomToLink.x
                    corridorY = roomToLink.y + math.floor(roomToLink.h * selectedCombination.corridorEntrance)
                    corridorW = selectedCombination.corridorLength
                    corridorH = selectedCombination.corridorThickness
                    roomCorridorX = roomW - 1
                    roomCorridorY = math.floor(roomH * selectedCombination.corridorExit)
                elseif selectedCombination.direction == room.Direction.RIGHT then
                    axis = room.Axis.HORIZONTAL
                    roomW = rotateRoom and selectedCombination.roomHeight or selectedCombination.roomWidth
                    roomH = rotateRoom and selectedCombination.roomWidth or selectedCombination.roomHeight
                    corridorX = roomToLink.x + roomToLink.w - 1
                    corridorY = roomToLink.y + math.floor(roomToLink.h * selectedCombination.corridorEntrance)
                    corridorW = selectedCombination.corridorLength
                    corridorH = selectedCombination.corridorThickness
                    roomCorridorX = 0
                    roomCorridorY = math.floor(roomH * selectedCombination.corridorExit)
                elseif selectedCombination.direction == room.Direction.UP then
                    axis = room.Axis.VERTICAL
                    roomW = selectedCombination.roomWidth
                    roomH = selectedCombination.roomHeight
                    corridorX = roomToLink.x + math.floor(roomToLink.w * selectedCombination.corridorEntrance)
                    corridorY = roomToLink.y
                    corridorW = selectedCombination.corridorThickness
                    corridorH = selectedCombination.corridorLength
                    roomCorridorX = math.floor(roomW * selectedCombination.corridorExit)
                    roomCorridorY = roomH - 1
                elseif selectedCombination.direction == room.Direction.DOWN then
                    axis = room.Axis.VERTICAL
                    roomW = selectedCombination.roomWidth
                    roomH = selectedCombination.roomHeight
                    corridorX = roomToLink.x + math.floor(roomToLink.w * selectedCombination.corridorEntrance)
                    corridorY = roomToLink.y + roomToLink.h - 1
                    corridorW = selectedCombination.corridorThickness
                    corridorH = selectedCombination.corridorLength
                    roomCorridorX = math.floor(roomW * selectedCombination.corridorExit)
                    roomCorridorY = 0
                end
                local newCorridor, newRoom = self:createLinkedRoom(roomToLink, axis, corridorX, corridorY,
                    corridorW, corridorH, roomCorridorX, roomCorridorY, roomW, roomH)
                if newCorridor then

                    local generatedRoomData = {
                        axis = axis,
                        corridorX = corridorX - roomToLink.x,
                        corridorY = corridorY - roomToLink.y,
                        roomCorridorX = roomCorridorX,
                        roomCorridorY = roomCorridorY,
                        combination = selectedCombination
                    }
                    
                    if selectedCombination.clearCorridor then
                        self:clearCorridor(newCorridor, roomToLink, newRoom, generatedRoomData)
                    end

                    return newCorridor, newRoom, generatedRoomData
                end
            end
        end,

        --- !!! Internal
        --- Interal method used to clear newly generayed corridors.
        --- @param self LibLevelGen.Segment
        --- @param newCorridor LibLevelGen.Room
        --- @param roomToLink LibLevelGen.Room
        --- @param newRoom LibLevelGen.Room
        --- @param generatedRoomData LibLegelGen.RoomGenResult
        clearCorridor = function(self, newCorridor, roomToLink, newRoom, generatedRoomData)
            newCorridor:fill {
                tileset = self.tileset,
                tileType = "Floor"
            }
            newCorridor:border {
                tileset = self.tileset,
                tileType = self.corridorBorder,
                sides = corridorWallsForAxis[generatedRoomData.axis]
            }

            local entranceTile = generatedRoomData.combination.entranceTileChances
                and self.instance:randWeightedChoice(generatedRoomData.combination.entranceTileChances)
                or "Floor"

            local exitTile = generatedRoomData.combination.exitTileChances
                and self.instance:randWeightedChoice(generatedRoomData.combination.exitTileChances)
                or "Floor"

            local clearCorridorThickness = generatedRoomData.combination.corridorThickness - 2
            if generatedRoomData.axis == room.Axis.HORIZONTAL then
                local initialYOffset = -math.floor(clearCorridorThickness / 2)
                local lastYOffset = math.floor(clearCorridorThickness / 2)
                if clearCorridorThickness % 2 == 0 then
                    lastYOffset = lastYOffset - 1
                end
                for i = initialYOffset, lastYOffset do
                    local tile1 = roomToLink:getTile(generatedRoomData.corridorX, generatedRoomData.corridorY + i)
                    local tile2 = newRoom:getTile(generatedRoomData.roomCorridorX, generatedRoomData.roomCorridorY + i)

                    tile1:convert(entranceTile)
                    tile2:convert(exitTile)

                    tile1:setIsEntrance()
                    tile2:setIsEntrance()
                end
            else
                local initialXOffset = -math.floor(clearCorridorThickness / 2)
                local lastXOffset = math.floor(clearCorridorThickness / 2)
                if clearCorridorThickness % 2 == 0 then
                    lastXOffset = lastXOffset - 1
                end
                for i = initialXOffset, lastXOffset do
                    local tile1 = roomToLink:getTile(generatedRoomData.corridorX + i, generatedRoomData.corridorY)
                    local tile2 = newRoom:getTile(generatedRoomData.roomCorridorX + i, generatedRoomData.roomCorridorY)

                    tile1:convert(entranceTile)
                    tile2:convert(exitTile)

                    tile1:setIsEntrance()
                    tile2:setIsEntrance()
                end
            end
        end,

        --- !!! Internal
        -- Creates a new area room and a new corridor room. The corridor room will link roomToLink to the new room.
        -- The room will be created with given width and height, in a way that connects it to the corridor.
        -- If the room or corridor cannot be placed in a way that doesn't overlap existing rooms, the function returns nil.
        -- If the room and corridor were successfully placed, they are both returned (corridor first, room second)
        -- Parameters:
        -- roomToLink: an existing room the corridor will be created from.
        -- axis (enum room.Axis): determines whether the corridor is horizontal or vertical
        -- corridorX: starting X coordinate of the corridor in the existing room. Expected to be on the edge.
        -- corridorY: starting Y coordinate of the corridor in the existing room. Expected to be on the edge.
        -- corridorW: width of the corridor room.
        -- corridorH: height of the corridor room.
        -- roomCorridorX: X coordinate of the corridor in the new room
        -- roomCorridorY: Y coordinate of the corridor in the new room
        -- roomW: width of the room to create.
        -- roomH: height of the room to create.
        --- @param self LibLevelGen.Segment
        --- @param roomToLink LibLevelGen.Room
        --- @param axis number
        --- @param corridorX number
        --- @param corridorY number
        --- @param corridorW number
        --- @param corridorH number
        --- @param roomCorridorX number
        --- @param roomCorridorY number
        --- @param roomW number
        --- @param roomH number
        --- @return LibLevelGen.Room newCorridor, LibLevelGen.Room newRoom
        createLinkedRoom = function(self, roomToLink, axis, corridorX, corridorY, corridorW, corridorH, roomCorridorX, roomCorridorY, roomW, roomH)
            local corridorRect
            if axis == room.Axis.VERTICAL then
                if corridorY == roomToLink.y then
                    corridorRect = {
                        x = corridorX - math.floor(corridorW / 2),
                        y = corridorY - corridorH,
                        h = corridorH,
                        w = corridorW
                    }
                elseif corridorY == roomToLink.y + roomToLink.h - 1 then
                    corridorRect = {
                        x = corridorX - math.floor(corridorW / 2),
                        y = corridorY + 1,
                        h = corridorH,
                        w = corridorW
                    }
                else
                    --dbg("invalid corridorY")
                end
            else
                if corridorX == roomToLink.x then
                    corridorRect = {
                        x = corridorX - corridorW,
                        y = corridorY - math.floor(corridorH / 2),
                        h = corridorH,
                        w = corridorW
                    }
                elseif corridorX == roomToLink.x + roomToLink.w - 1 then
                    corridorRect = {
                        x = corridorX + 1,
                        y = corridorY - math.floor(corridorH / 2),
                        h = corridorH,
                        w = corridorW
                    }
                end
            end

            if not corridorRect or self:rectOverlapsWithAnyRoom(corridorRect) then
                return nil, nil
            end

            local roomRect
            if axis == room.Axis.VERTICAL then
                if roomCorridorY == roomH - 1 then
                    -- Room above corridor
                    roomRect = {
                        x = corridorRect.x + math.floor(corridorRect.w / 2) - roomCorridorX,
                        y = corridorRect.y - roomH,
                        w = roomW,
                        h = roomH
                    }
                elseif roomCorridorY == 0 then
                    -- Room below corridor
                    roomRect = {
                        x = corridorRect.x + math.floor(corridorRect.w / 2) - roomCorridorX,
                        y = corridorRect.y + corridorRect.h,
                        w = roomW,
                        h = roomH
                    }
                else
                    --dbg("roomCorridorY does not link to the corridor")
                end
            else
                if roomCorridorX == roomW - 1 then
                    -- Room to the left of corridor
                    roomRect = {
                        x = corridorRect.x - roomW,
                        y = corridorRect.y + math.floor(corridorRect.h / 2) - roomCorridorY,
                        w = roomW,
                        h = roomH
                    }
                elseif roomCorridorX == 0 then
                    -- Room to the right of corridor
                    roomRect = {
                        x = corridorRect.x + corridorRect.w,
                        y = corridorRect.y + math.floor(corridorRect.h / 2) - roomCorridorY,
                        w = roomW,
                        h = roomH
                    }
                else
                    --dbg("roomCorridorX does not link to the corridor")
                end
            end

            if not roomRect or self:rectOverlapsWithAnyRoom(roomRect) then
                return nil, nil
            end

            local corridorRoom = self:createRoom(corridorRect.x, corridorRect.y, corridorRect.w, corridorRect.h, room.Type.CORRIDOR)

            local newRoom = self:createRoom(roomRect.x, roomRect.y, roomRect.w, roomRect.h, room.Type.REGULAR)

            local linkSize, shiftX, shiftY
            if axis == room.Axis.VERTICAL then
                linkSize = corridorW
                shiftX = -math.floor(corridorRect.w / 2)
                shiftY = 0
            else
                linkSize = corridorH
                shiftX = 0
                shiftY = -math.floor(corridorRect.h / 2)
            end

            newRoom.links[#newRoom.links+1] = {
                x = roomCorridorX + shiftX,
                y = roomCorridorY + shiftY,
                centerX = roomCorridorX,
                centerY = roomCorridorY,
                axis = axis,
                size = linkSize,
                room = corridorRoom
            }

            roomToLink.links[#roomToLink.links+1] = {
                x = corridorX - roomToLink.x + shiftX,
                y = corridorY - roomToLink.y + shiftY,
                centerX = corridorX - roomToLink.x,
                centerY = corridorY - roomToLink.y,
                axis = axis,
                size = linkSize,
                room = corridorRoom
            }

            local corridorLink1 = axis == room.Axis.HORIZONTAL
                and (roomToLink.x < newRoom.x and roomToLink or newRoom)
                or (roomToLink.y < newRoom.y and roomToLink or newRoom)

            -- TODO verify centerX/centerY for the corridor links
            corridorRoom.links[#corridorRoom.links+1] = {
                x = 0,
                y = 0,
                centerX = 0 - shiftX,
                centerY = 0 - shiftY,
                axis = axis,
                size = linkSize,
                room = corridorLink1
            }
            corridorRoom.links[#corridorRoom.links+1] = {
                x = axis == room.Axis.HORIZONTAL and (corridorRoom.w - 1) or 0,
                y = axis == room.Axis.VERTICAL and (corridorRoom.h - 1) or 0,
                centerX = (axis == room.Axis.HORIZONTAL and (corridorRoom.w - 1) or 0) - shiftX,
                centerY = (axis == room.Axis.VERTICAL and (corridorRoom.h - 1) or 0) - shiftY,
                axis = axis,
                size = linkSize,
                room = corridorLink1 == roomToLink and newRoom or roomToLink
            }

            return corridorRoom, newRoom
        end,

        --- Checks whether the given rect overlaps with any room, and returns the 1st room that overlapped.
        --- @param self LibLevelGen.Segment
        --- @param rect LibLevelGen.Rect
        --- @return LibLevelGen.Room | nil
        rectOverlapsWithAnyRoom = function(self, rect)
            for i = 1, #self.rooms do
                local room = self.rooms[i]
                if rect.x <= (room.x + room.w - 1) and
                   (rect.x + rect.w - 1) >= room.x and
                   rect.y <= (room.y + room.h - 1) and
                   (rect.y + rect.h - 1) >= room.y
                then
                    return room
                end
            end
        end,

        --- Return all rooms that overlap with the given rect.
        --- @param self LibLevelGen.Segment
        --- @param rect LibLevelGen.Rect
        --- @return LibLevelGen.Room[]
        rectOverlappedRooms = function(self, rect)
            --- @type LibLevelGen.Room[]
            local res = {}
            for i = 1, #self.rooms do
                local room = self.rooms[i]
                if rect.x <= (room.x + room.w - 1) and
                   (rect.x + rect.w - 1) >= room.x and
                   rect.y <= (room.y + room.h - 1) and
                   (rect.y + rect.h - 1) >= room.y
                then
                    res[#res+1] = room
                end
            end
            return res
        end,

        --- Check whether the given rect is within bounds of the segment.
        --- @param self LibLevelGen.Segment
        --- @param rect LibLevelGen.Rect
        --- @param bounds number[]
        --- @return boolean
        rectInBounds = function(self, rect, bounds)
            if not bounds then
                bounds = self:determineBounds()
            end
            local padded = bounds[5]
            return
                rect.x >= bounds[1]+padded and rect.x + rect.w <= bounds[1] + bounds[3] - 1 - padded and
                rect.y >= bounds[2]+padded and rect.y + rect.h <= bounds[2] + bounds[4] - 1 - padded
        end,

        ---@class LibLevelGen.Overlaps
        ---@field top LibLevelGen.Room[]
        ---@field bottom LibLevelGen.Room[]
        ---@field left LibLevelGen.Room[]
        ---@field right LibLevelGen.Room[]

        ---@class LibLevelGen.OverlappedRect : LibLevelGen.Rect
        ---@field overlaps LibLevelGen.Overlaps

        --- Find rects between rooms of the segment, of minimum width and height.
        --- @param self LibLevelGen.Segment
        --- @param minWidth number
        --- @param minHeight number
        --- @return LibLevelGen.OverlappedRect[]
        findBetweenRoomRects = function(self, minWidth, minHeight)
            local res = {}
            local bounds = self:determineBounds()
            local originalRoomListSize = #self.rooms
            for x = bounds[1] + 1, bounds[3] - 2 do
                for y = bounds[2] + 1, bounds[4] - 2 do
                    local possibleRect = {
                        x = x,
                        y = y,
                        w = minWidth,
                        h = minHeight,
                        fakeRoom = true
                    }
                    local overlap = self:rectOverlapsWithAnyRoom(possibleRect)
                    if not overlap and self:rectInBounds(possibleRect, bounds) then
                        possibleRect.overlaps = {}
                        repeat
                            possibleRect.w = possibleRect.w + 1
                        until self:rectOverlapsWithAnyRoom(possibleRect) or not self:rectInBounds(possibleRect, bounds)
                        possibleRect.w = possibleRect.w - 1
                        repeat
                            possibleRect.h = possibleRect.h + 1
                        until self:rectOverlapsWithAnyRoom(possibleRect) or not self:rectInBounds(possibleRect, bounds)
                        possibleRect.h = possibleRect.h - 1

                        res[#res+1] = possibleRect
                        self.rooms[#self.rooms+1] = possibleRect -- Temporary addition
                    end
                end
            end
            while originalRoomListSize < #self.rooms do
                table.remove(self.rooms, #self.rooms)
            end
            for i = 1, #res do
                local possibleRect = res[i]
                possibleRect.h = possibleRect.h + 1
                possibleRect.overlaps.bottom = self:rectOverlappedRooms(possibleRect)
                possibleRect.h = possibleRect.h - 1

                possibleRect.w = possibleRect.w + 1
                possibleRect.overlaps.right = self:rectOverlappedRooms(possibleRect)
                possibleRect.w = possibleRect.w - 1

                possibleRect.x = possibleRect.x - 1
                possibleRect.overlaps.left = self:rectOverlappedRooms(possibleRect)
                possibleRect.x = possibleRect.x + 1

                possibleRect.y = possibleRect.y - 1
                possibleRect.overlaps.top = self:rectOverlappedRooms(possibleRect)
                possibleRect.y = possibleRect.y + 1
            end
            return res
        end,

        --- Counts how many sides of an overlappedRect are adjacent to a room, including other overlapped rects.
        --- @param self LibLevelGen.Segment
        --- @param betweenRoomRect LibLevelGen.OverlappedRect
        --- @return boolean
        beetweenRoomRectSidesWithAdjecentRooms = function (self, betweenRoomRect)
            return
                (#betweenRoomRect.overlaps.top > 0 and 1 or 0) +
                (#betweenRoomRect.overlaps.bottom > 0 and 1 or 0) +
                (#betweenRoomRect.overlaps.left > 0 and 1 or 0) +
                (#betweenRoomRect.overlaps.right > 0 and 1 or 0)
        end,

        --- Counts how many "fake rooms" (other overlapped rects) are adjacent to this overlapped rect.
        --- @param self LibLevelGen.Segment
        --- @param betweenRect LibLevelGen.OverlappedRect
        --- @return number
        beetweenRoomRectAdjecentFakeRooms = function(self, betweenRect)
            local topFakeRooms = 0
            for i = 1, #betweenRect.overlaps.top do
                if betweenRect.overlaps.top[i].fakeRoom then
                    topFakeRooms = topFakeRooms + 1
                else
                    topFakeRooms = 0
                    break
                end
            end
            local bottomFakeRooms = 0
            for i = 1, #betweenRect.overlaps.bottom do
                if betweenRect.overlaps.bottom[i].fakeRoom then
                    bottomFakeRooms = bottomFakeRooms + 1
                else
                    bottomFakeRooms = 0
                    break
                end
            end
            local rightFakeRooms = 0
            for i = 1, #betweenRect.overlaps.right do
                if betweenRect.overlaps.right[i].fakeRoom then
                    rightFakeRooms = rightFakeRooms + 1
                else
                    rightFakeRooms = 0
                    break
                end
            end
            local leftFakeRooms = 0
            for i = 1, #betweenRect.overlaps.left do
                if betweenRect.overlaps.left[i].fakeRoom then
                    leftFakeRooms = leftFakeRooms + 1
                else
                    leftFakeRooms = 0
                    break
                end
            end
            return topFakeRooms + bottomFakeRooms + rightFakeRooms + leftFakeRooms
        end,

        --- Detemine bounds of the segment.
        --- @param self LibLevelGen.Segment
        --- @return number[]
        determineBounds = function(self)
            if #self.rooms == 0 then
                return nil
            end
            local minX = math.huge
            local minY = math.huge
            local maxX = -math.huge
            local maxY = -math.huge
            for i = 1, #self.rooms do
                local room = self.rooms[i]
                if room.x < minX then
                    minX = room.x
                end
                if room.y < minY then
                    minY = room.y
                end
                if room.x + room.w - 1 > maxX then
                    maxX = room.x + room.w - 1
                end
                if room.y + room.h - 1 > maxY then
                    maxY = room.y + room.h - 1
                end
            end

            -- Extended sligtly for the level borders
            -- +1 is for level border
            local n = self.padding.count + 1
            return {minX - n, minY - n, maxX - minX + 1 + n*2, maxY - minY + 1 + n*2, n}
        end,

        --- !!! Internal
        --- Serializes tiles within this segment.
        --- @param self LibLevelGen.Segment
        --- @param bounds number[]
        --- @param tileMapping table
        --- @return number[] tileArray, number[] wireArray
        serializeTiles = function(self, bounds, tileMapping)
            local tileArray = {}
            local wireArray = {}

            local segmentX = bounds[1]
            local segmentY = bounds[2]
            local w = bounds[3]
            local h = bounds[4]
            local pad = bounds[5]
            local tileCount = w * h
            for i = 1, tileCount do
                tileArray[i] = 0 -- 0 is void
            end
            for i = 1, #self.rooms do
                local room = self.rooms[i]
                for x = 0, room.w - 1 do
                    for y = 0, room.h - 1 do
                        local tile = room:getTile(x, y)
                        local index = w * (room.y + y - segmentY) + (room.x + x - segmentX) + 1
                        if tile.wire then
                            wireArray[index] = tile.wire
                        end
                        local mappedTile = tile and tileMapping[tile.tileset][tile.type] or 0
                        tileArray[index] = mappedTile
                    end
                end

                -- Place level borders around the room on void tiles
                for j = 1, pad do
                    local offset = j - 1
                    -- Top
                    for x = -j, room.w - 1 + j do
                        local index = w * (room.y - 1 - segmentY - offset) + (room.x + x - segmentX) + 1
                        local tile = tileArray[index]
                        if tile == 0 or tile == 1 then
                            if j == pad then
                                tileArray[index] = 1
                            else
                                tileArray[index] = room.padding or self.padding
                            end
                        end
                    end
                    -- Bottom
                    for x = -j, room.w - 1 + j do
                        local index = w * (room.y + room.h - segmentY + offset) + (room.x + x - segmentX) + 1
                        local tile = tileArray[index]
                        if tile == 0 or tile == 1 then
                            if j == pad then
                                tileArray[index] = 1
                            else
                                tileArray[index] = room.padding or self.padding
                            end
                        end
                    end
                    -- Left
                    for y = -j, room.h - 1 + j do
                        local index = w * (room.y + y - segmentY) + (room.x - 1 - segmentX - offset) + 1
                        local tile = tileArray[index]
                        if tile == 0 or tile == 1 then
                            if j == pad then
                                tileArray[index] = 1
                            else
                                tileArray[index] = room.padding or self.padding
                            end
                        end
                    end
                    -- Right
                    for y = -j, room.h - 1 + j do
                        local index = w * (room.y + y - segmentY) + (room.x + room.w - segmentX + offset) + 1
                        local tile = tileArray[index]
                        if tile == 0 or tile == 1 then
                            if j == pad then
                                tileArray[index] = 1
                            else
                                tileArray[index] = room.padding or self.padding
                            end
                        end
                    end
                end
            end

            for i = 1, #tileArray do
                if type(tileArray[i]) == "table" then
                    local tile = self:getRandomPaddingTile(tileArray[i])
                    tileArray[i] = tile and tileMapping[tile.tileset][tile.type] or 1
                end
            end

            return tileArray, wireArray
        end,

        --- !!! Internal
        --- Serializes this segment.
        --- @param self LibLevelGen.Segment
        --- @param tileMapping table
        --- @param previousSegmentBounds number[]
        serialize = function(self, tileMapping, previousSegmentBounds)
            local bounds = self:determineBounds()
            if not bounds then
                return nil
            end

            local tiles, wires = self:serializeTiles(bounds, tileMapping)

            local offset
            if previousSegmentBounds then
                local newX = previousSegmentBounds[1] - bounds[3] - 1
                local newY = previousSegmentBounds[2] - bounds[4] - 1
                offset = {
                    newX - bounds[1],
                    newY - bounds[2]
                }
                bounds[1] = newX
                bounds[2] = newY
            else
                offset = {0, 0}
            end

            return {
                bounds = bounds,
                tiles = tiles,
                wires = #wires > 0 and wires
            }, offset
        end,
    }
}

--- @param instance LibLegelGen.Instance
---@return LibLevelGen.Segment
function segment.new(instance)
    local newSegment = {}
    setmetatable(newSegment, mt)
    newSegment.instance = instance
    newSegment:init()
    return newSegment
end

-- This function returns combinations to be used with createRandLinkedRoom.
-- Parameters:
-- direction: table of possible directions (enum room.Direction) the corridor can be generated in
-- corridorEntrance: table of positive corridor entrances; float between 0-1, based on which the entrance point will be calculated from length of wall the corridor is attached to
-- corridorExit: table of positive corridor exits; float between 0-1, based on which the exit point will be calculated from length of wall from the new room the corridor is attached to
-- corridorThickness: table of possible corridor thickness values, which may be used as width or height depending on the corridor axis
-- corridorLength: table of possible corridor length values, which may be used as width or height depending on the corridor axis
-- roomWidth: table of possible new room widths
-- roomHeight: table of possible new room heights
-- clearCorridor (default: true): whether the appriopriate tiles should be set to floor, in order to make it possible to actually go through the corridor
-- The following properties only apply if clearCorridor is non-false.
-- entranceTileChances: table like {Door = 0.5, Floor = 0.5} that contains what the corridor entrance tile can be set to. If not given, will always default to Floor.
-- exitTileChances: table of same format as above, but for the corridor exit tile. If not given, defaults to Floor.

---@param parameters LibLevelGen.RoomGenCombinations
---@return LibLevelGen.RoomGenCombination[]
function segment.createRandLinkedRoomParameterCombinations(parameters)
    ---@type LibLevelGen.RoomGenCombination[]
    local res = {}
    local clearCorridor = parameters.clearCorridor
    if clearCorridor == nil then
        clearCorridor = true
    end
    for _, dir in ipairs(parameters.direction) do
        for _, corridorThickness in ipairs(parameters.corridorThickness) do
            for _, corridorLength in ipairs(parameters.corridorLength) do
                for _, roomWidth in ipairs(parameters.roomWidth) do
                    for _, roomHeight in ipairs(parameters.roomHeight) do
                        for _, corridorEntrance in ipairs(parameters.corridorEntrance) do
                            for _, corridorExit in ipairs(parameters.corridorExit) do
                                res[#res+1] = {
                                    direction = dir,
                                    corridorEntrance = corridorEntrance,
                                    corridorExit = corridorExit,
                                    corridorThickness = corridorThickness,
                                    corridorLength = corridorLength,
                                    roomWidth = roomWidth,
                                    roomHeight = roomHeight,
                                    clearCorridor = clearCorridor,
                                    entranceTileChances = parameters.entranceTileChances,
                                    exitTileChances = parameters.exitTileChances
                                }
                            end
                        end
                    end
                end
            end
        end
    end
    return res
end

return segment
