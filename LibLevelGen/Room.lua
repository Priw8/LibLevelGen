local action = require "necro.game.system.Action"
local collision = require "necro.game.tile.Collision"
local currency = require "necro.game.item.Currency"
local tileTypes = require "necro.game.tile.TileTypes"
local itemGeneration = require "necro.game.item.ItemGeneration"

local enum = require "system.utils.Enum"
local util = require "system.utils.Utilities"

local entity = require "LibLevelGen.Entity"
local levelGenUtil = require "LibLevelGen.Util"
local loot = require "LibLevelGen.Loot"

local tr = levelGenUtil.TileRequirements

local room = {}

--- Used to determine whether a room was generated vertically or horizontally.
room.Axis = enum.sequence {
    VERTICAL = 1,
    HORIZONTAL = 2,
}

--- @rawbegin room.Direction
room.Direction = action.Direction -- From necro.game.system.Action
--- @rawend

--- @rawbegin room.DirectionVector
room.DirectionVector = {
    [room.Direction.LEFT] = {-1, 0},
    [room.Direction.RIGHT] = {1, 0},
    [room.Direction.UP] = {0, -1},
    [room.Direction.DOWN] = {0, 1}
}
--- @rawend

--- Flags which determine the properties of the room
room.Flag = enum.bitmask {
    NONE = 0,
    ALLOW_ENEMY = 1,
    ALLOW_CHEST = 2,
    ALLOW_CRATE = 3,
    ALLOW_TRAP = 4,
    ALLOW_TRAVELRUNE = 5,
    ALLOW_SHRINE = 6,
    ALLOW_TORCH = 7,
    ALLOW_TILE_CONVERSION = 8,
    ALLOW_SECRET_FILL = 9,
    EXIT = 10,
    STARTING = 11
}

local f = room.Flag

--- Helper enum containing common `room.Flag` bitmasks.
room.Type = enum.sequence {
    REGULAR = room.Flag.mask(f.ALLOW_ENEMY, f.ALLOW_CHEST, f.ALLOW_CRATE, f.ALLOW_TRAP, f.ALLOW_TRAVELRUNE, f.ALLOW_SHRINE, f.ALLOW_TORCH, f.ALLOW_TILE_CONVERSION),
    STARTING = room.Flag.mask(f.ALLOW_TRAVELRUNE, f.ALLOW_TORCH, f.ALLOW_TILE_CONVERSION, f.STARTING),
    EXIT = room.Flag.mask(f.ALLOW_ENEMY, f.ALLOW_TRAVELRUNE, f.ALLOW_TORCH, f.ALLOW_TILE_CONVERSION, f.EXIT),
    CORRIDOR = f.ALLOW_TILE_CONVERSION,
    SECRET = room.Flag.mask(f.ALLOW_TILE_CONVERSION, f.ALLOW_SECRET_FILL),
    SHOP = f.NONE,
    SECRET_SHOP = f.NONE,
    OTHER = f.NONE,
}

local fallbackTileTypes = {
    ["Door"] = "DoorHorizontal",
    ["MetalDoor"] = "MetalDoorHorizontal",
    ["DoorVertical"] = "DoorHorizontal",
    ["MetalDoorHorizontal"] = "MetalDoorHorizontal"
}

local specialTileID = {
    Void = -1
}

local specialTileInfo = {
    [specialTileID.Void] = {
        isVoid = true
    }
}

local function lookUpTileID(tileType, zoneID)
    return specialTileID[tileType] or tileTypes.lookUpTileID(tileType, zoneID)
end

local function getTileInfo(tileID)
    return specialTileInfo[tileID] or tileTypes.getTileInfo(tileID)
end

--- @rawbegin Tribool alias
--- @alias TriBool boolean | nil
--- @rawend

--- @class LibLevelGen.TileRequirements
--- Interface used for selecting tiles based on the given requirement.
--- Fields of the `Tribool` type can be `nil`, `true` or `false`.
--- When a `Tribool` is `nil` the condition is not checked, and if it's a boolean then
--- the condition check result must match the value specified.
--- @field isFloor TriBool # Whether the tile is a floor.
--- @field isWall TriBool # Whether the tile is a wall.
--- @field levelExit TriBool # Whether the tile is the level exit.
--- @field hasWallTorch TriBool # Whether the tile has a wall torch.
--- @field hasEntity TriBool # Whether the tile has an entity.
--- @field tileType string | nil # Required tile type string name.
--- @field nearFloor TriBool # Whether the tile is near a floor tile, checked within this room only.
--- @field nearWall TriBool # Whether the tile is near a wall tile, checked within this room only.
--- @field nearBorder TriBool # Whether the tile is near the room border.
--- @field filter fun(room: LibLevelGen.Room, tile: LibLevelGen.Tile) | nil # Filter function, if it returns true the condition check passes.
--- @field notCollision number | nil # Collision (from `necro.game.tile.Collision`) bits which must all NOT be set.
--- @field collision number | nil # Collision (from `necro.game.tile.Collision`) bits which must all be set.
--- @field rect LibLevelGen.Rect | nil # Rect the tile must be in.
--- @field notRect LibLevelGen.Rect | nil # Rect the tile must NOT be in.
--- @field wire TriBool # Whether the tile has an electric wile.
--- @field info table | nil # Require the tile to have a specific `tileInfo` (from `necro.game.tile.TileTypes`)
--- @field entrance TriBool # Whether the tile is the entrance to the room (directly next to the corridor).
--- @field adjacentEntrance TriBool # Whether the tile is adjacent to the entrance.
--- @field directlyAdjacentEntrance TriBool # Whether the tile is adjacent to the entrance, but not diagonally.
--- @field noWallTallSprite TriBool # Whether the tile below the checked tile is a wall.

local tileMt = {
    --- @class LibLevelGen.Tile
    --- Object representing a single tile. This will be moved to a separate module in the future.
    --- @field tileset string # Tileset the tile uses.
    --- @field type string # Tile type the tile uses.
    --- @field info table # TileInfo of the tile (from `necro.game.tile.TileTypes`).
    --- @field wire integer # The wire connectivity bitmask.
    --- @field hasEntity boolean # Whether the tile has an entity.
    --- @field entities LibLevelGen.Entity[] # List of entities on this tile.
    --- @field hasWallTorch boolean # Whether the tile has a wall torch.
    --- @field x number # X coordinate of the tile in the room.
    --- @field y number # Y coordinate of the tile in the room.
    --- @field rx number # X coordinate of the tile in the segment.
    --- @field ry number # Y coordinate of the tile in the segment.
    --- @field adjacentEntrance boolean # Whether the tile is adjacent to an entrance.
    --- @field directlyAdjacentEntrance boolean # Whether the tile is adjacent to an entrance, but not diagonally.
    --- @field isEntrance boolean # Whether the tile is the entrance to the room (directly next to the corridor).
    --- @field room LibLevelGen.Room # Room this tile belongs to.
    --- @field instance LibLegelGen.Instance # Instance this tile belongs to.
    __index = {
        adjacentEntrance = false,
        directlyAdjacentEntrance = false,
        isEntrance = false,

        --- Convert the tile to a different type.
        --- @param self LibLevelGen.Tile # The tile
        --- @param tileType string # TileType to convert to.
        convert = function(self, tileType)
            tileType = fallbackTileTypes[tileType] and fallbackTileTypes[tileType] or tileType
            self.type = tileType
            self.info = getTileInfo(lookUpTileID(tileType, tileTypes.lookUpZoneID(self.tileset)))
            if not self.info then
                error("invalid tile: " .. self.tileset .. ":" .. tileType)
            end
        end,

        --- Set the isEntrance flag on the tile and adjacentEntrance flags on nearby tiles.
        --- @param self LibLevelGen.Tile # The tile
        setIsEntrance = function(self)
            for x = -1, 1 do
                for y = -1, 1 do
                    local adjacentTile = self.room:getTile(self.x + x, self.y + y)
                    if adjacentTile then
                        adjacentTile.adjacentEntrance = true
                        if x == 0 or y == 0 then
                            adjacentTile.directlyAdjacentEntrance = true
                        end
                    end
                end
            end
            self.isEntrance = true
        end,

        --- Place entity of given type and level on the tile.
        --- @param self LibLevelGen.Tile # The tile.
        --- @param entityOrType string | LibLevelGen.Entity # Entity type or existing entity.
        --- @param level? number # Entity level, ignored if existing entity is passed in the previous argument.
        --- @return LibLevelGen.Entity
        placeEntity = function(self, entityOrType, level)
            if not entityOrType then
                error("Tile:placeEntity: entity is nil")
            end

            self.hasEntity = true
            local e = entityOrType
            if type(entityOrType) == "string" then
                e = entity.new(self.instance, entityOrType, self.x, self.y, level)
            else
                if e.x ~= -1 or e.y ~= -1 then
                    error("Tile:placeEntity: attempt to place entity that has already been placed")
                end
                e.x = self.x
                e.y = self.y
            end
            self.entities[#self.entities+1] = e
            return e
        end,

        --- Check if the tile meets specified requirements.
        --- @param self LibLevelGen.Tile # The tile.
        --- @param requirements LibLevelGen.TileRequirements # Requirements the tile has to meet.
        meetsRequirements = function(self, requirements)
            -- TODO: make this work... in a more reasonable way
            local tileInfo = self.info
            return
                {tileInfo.isVoid ~= true} and
                (requirements.isFloor == nil or (tileInfo.isFloor or false) == requirements.isFloor) and
                (requirements.isWall == nil or (tileInfo.isWall or false) == requirements.isWall) and
                (requirements.levelExit == nil or (tileInfo.levelExit or false) == requirements.levelExit) and
                (requirements.hasWallTorch == nil or self.hasWallTorch == requirements.hasWallTorch) and
                (requirements.hasEntity == nil or self.hasEntity == requirements.hasEntity) and
                (requirements.tileType == nil or self.type == requirements.tileType) and
                (requirements.nearFloor == nil or self.room:isTileNearFloor(self) == requirements.nearFloor) and
                (requirements.nearWall == nil or self.room:isTileNearWall(self) == requirements.nearWall) and
                (requirements.nearBorder == nil or self.room:isTileNearBorder(self) == requirements.nearBorder) and
                (requirements.filter == nil or requirements.filter(self.room, self)) and
                (requirements.notCollision == nil or bit.band(tileInfo.collision, requirements.notCollision) == 0) and
                (requirements.collision == nil or bit.band(tileInfo.collision, requirements.collision) ~= 0) and
                (requirements.rect == nil or self:inRect(requirements.rect)) and
                (requirements.notRect == nil or not self:inRect(requirements.notRect)) and
                (requirements.wire == nil or (tileInfo.wire or false) == requirements.wire) and
                (requirements.info == nil or requirements.info == tileInfo) and
                (requirements.entrance == nil or requirements.entrance == self.isEntrance) and
                (requirements.adjacentEntrance == nil or requirements.adjacentEntrance == self.adjacentEntrance) and
                (requirements.directlyAdjacentEntrance == nil or requirements.directlyAdjacentEntrance == self.directlyAdjacentEntrance) and
                (requirements.noWallTallSprite == nil or self:tileBelowIsWall() ~= requirements.noWallTallSprite) and
                --- TODO: delete this, there is requirements tileType already
                --- @diagnostic disable-next-line
                (requirements.type == nil or requirements.type == self.type)
        end,

        --- Whether a wall tile or the room border is below this tile.
        --- @param self LibLevelGen.Tile # The tile.
        tileBelowIsWall = function(self)
            local tileBelow = self.room:getTile(self.x, self.y + 1)
            return not tileBelow or tileBelow.info.isWall
        end,

        --- Check if the tile is within a given rect in the room.
        --- @param self LibLevelGen.Tile # The tile.
        --- @param rect LibLevelGen.Rect # Rect to check against.
        inRect = function(self, rect)
            return self.x >= rect.x and self.x < rect.x + rect.w
                and self.y >= rect.y and self.y < rect.y + rect.h
        end
    }
}

--- @class LibLevelGen.RoomLinkData
--- Data which represents where a room connects to other rooms.
--- @field x number # Top-left X coordinate of the connection.
--- @field y number # Top-left Y coordinate of the connection.
--- @field centerX number # X center of the connection.
--- @field centerY number # Y center of the connection.
--- @field axis number # `LibLevelGen.Axis`
--- @field size number # How long the connection is, it's either width or height, depending on the axis.
--- @field room LibLevelGen.Room # The room that the link leads to.

--- @class LibLevelGen.RoomLinks
--- Object containing room links for every side of the room.
--- @field top LibLevelGen.RoomLinkData[] # Link data of the top side of the room.
--- @field bottom LibLevelGen.RoomLinkData[] # Link data of the bottom side of the room.
--- @field left LibLevelGen.RoomLinkData[] # Link data of the left side of the room.
--- @field right LibLevelGen.RoomLinkData[] # Link data of the right side of the room.

--- @class LibLevelGen.Point
--- Simple point interface.
--- @field x number # x
--- @field y number # y

local mt = {
    --- @class LibLevelGen.Room
    --- Object representing a room.
    --- @field x number # X coordinate of the room's top left corner within the segment.
    --- @field y number # Y coordinate of the room's top left corner within the segment.
    --- @field w number # Width of the room.
    --- @field h number # Height of the room.
    --- @field flags number # Room flags (from `room.Flag`).
    --- @field links LibLevelGen.RoomLinkData[] # Information about the rooms adjacent to this room and where exactly the entrances are.
    --- @field padding LibLevelGen.PaddingData | nil # Padding override to use instead of the segment-defined one.
    --- @field tiles LibLevelGen.Tile[] # Array of tiles in this room.
    --- @field exit LibLevelGen.Point | nil # Coordinates of exit stairs within this room (if they exist).
    --- @field instance LibLegelGen.Instance # Instance this room belongs to.
    --- @field segment LibLevelGen.Segment # Segment this table belongs to.
    --- @field userData table # Table where you can put whatever you want safely.
    __index = {
        x = 0,
        y = 0,
        w = 0,
        h = 0,
        flags = 0,

        --- !!! Internal
        --- Initializes the room.
        --- @param self LibLevelGen.Room
        --- @param x number
        --- @param y number
        --- @param w number
        --- @param h number
        --- @param flags number
        init = function(self, x, y, w, h, flags)
            self.x = x
            self.y = y
            self.w = w
            self.h = h
            self.flags = flags or room.Flag.NONE
            self.tiles = {}
            self.links = {}
            self.userData = {}

            -- Always putting some tiles is a reasonable behaviour, given that not having any tiles causes errors...
            self:fill("Floor")
            self:border(self.segment.roomBorder)
        end,

        --- Adds new flags on top of existing flags.
        --- @param self LibLevelGen.Room # The room.
        --- @param newFlags number # The new flags.
        setFlags = function(self, newFlags)
            self.flags = room.Flag.mask(self.flags, newFlags)
        end,

        --- Removes the given flags.
        --- @param self LibLevelGen.Room # The room.
        --- @param flagsToClear number # Flags to remove.
        clearFlags = function(self, flagsToClear)
            self.flags = room.Flag.unmask(self.flags, flagsToClear)
        end,

        --- Removes all flags apart from the given ones.
        --- @param self LibLevelGen.Room # the room.
        --- @param flags number # Flags to use as a mask.
        maskFlags = function(self, flags)
            self.flags = room.Flag.unmask(self.flags, bit.bnot(flags))
        end,

        --- Checks if the given flags are set.
        --- @param self LibLevelGen.Room # The room.
        --- @param flags number # Flags to check.
        checkFlags = function(self, flags)
            return room.Flag.check(self.flags, flags)
        end,

        --- Set the padding that will be used instead of the one specified by the segment.
        --- @param self LibLevelGen.Room # The room.
        --- @param data LibLevelGen.PaddingData # Padding definition object.
        setPaddingTileOverride = function(self, data)
            self.padding = data
        end,

        --- Get bounds of the room (in table `{x, y, w, h}`).
        --- @param self LibLevelGen.Room # The room.
        --- @return number[]
        getBounds = function(self)
            return {self.x, self.y, self.w, self.h}
        end,

        --- Get rect of the room.
        --- @param self LibLevelGen.Room # The room.
        --- @return LibLevelGen.Rect
        getRect = function(self)
            return {
                x = self.x,
                y = self.y,
                w = self.w,
                h = self.h,
            }
        end,

        --- !!! Internal
        --- Get tile index within the tile array.
        --- @param self LibLevelGen.Room
        --- @param x number
        --- @param y number
        indexTile = function(self, x, y)
            return self.w * y + x + 1
        end,

        --- Set tile on the given coordinates. If tileset is not specified, the default one of the segment is used.
        --- @param self LibLevelGen.Room # The room.
        --- @param x number # X coordinates of the tile.
        --- @param y number # Y coordinates of the tile.
        --- @param tileType string # The tile type
        --- @param tileset? string # The tileset, if not specified defaults to the tileset of the segment.
        setTile = function(self, x, y, tileType, tileset)
            -- TODO: dejankify tile creation (for real tho)
            -- Especially considering that all tiles are initialized now...
            tileType = fallbackTileTypes[tileType] and fallbackTileTypes[tileType] or tileType

            tileset = tileset or self.segment.tileset
            if x < 0 or x >= self.w or y < 0 or y >= self.h then
                return nil
            end
            local prevTile = self.tiles[self:indexTile(x, y)]
            local tile = {
                tileset = tileset,
                type = tileType,
                info = getTileInfo(lookUpTileID(tileType, tileTypes.lookUpZoneID(tileset))),
                hasEntity = (prevTile and prevTile.hasEntity) == true,
                entities = (prevTile and prevTile.entities or {}),
                hasWallTorch = (prevTile and prevTile.hasWallTorch) == true,
                x = x,
                y = y,
                rx = x + self.x,
                ry = y + self.y,
                room = self,
                instance = self.instance
            }
            if not tile.info then
                error("invalid tile: " .. tileset .. ":" .. tileType)
            end
            setmetatable(tile, tileMt)
            self.tiles[self:indexTile(x, y)] = tile
            return tile
        end,

        --- Get tile on given coordinates
        --- @param self LibLevelGen.Room # The room.
        --- @param x number # X coordinates of the tile.
        --- @param y number # Y coordinates of the tile.
        --- @return LibLevelGen.Tile
        getTile = function(self, x, y)
            if x < 0 or x >= self.w or y < 0 or y >= self.h then
                return nil
            end
            return self.tiles[self:indexTile(x, y)]
        end,

        --- Delete the given tile (set it to Void pseudotile)
        --- @param self LibLevelGen.Room # The room.
        --- @param x number # X coordinates of the tile.
        --- @param y number # Y coordinates of the tile.
        deleteTile = function(self, x, y)
            self:setTile(x, y, "Void")
        end,

        --- Checks if the given tile (assumed to be within this room) is near a wall tile.
        --- @param self LibLevelGen.Room # The room.
        --- @param tile LibLevelGen.Tile # The tile to check.
        --- @return boolean
        isTileNearWall = function(self, tile)
            local up = self:getTile(tile.x, tile.y - 1)
            if up and up.info.isWall then
                return true
            end
            local down = self:getTile(tile.x, tile.y + 1)
            if down and down.info.isWall then
                return true
            end
            local left = self:getTile(tile.x - 1, tile.y)
            if left and left.info.isWall then
                return true
            end
            local right = self:getTile(tile.x + 1, tile.y)
            if right and right.info.isWall then
                return true
            end
            return false
        end,

        --- Checks if the given tile (assumed to be within this room) is near a floor tile.
        --- @param self LibLevelGen.Room # The room.
        --- @param tile LibLevelGen.Tile # The tile to check.
        --- @return boolean
        isTileNearFloor = function(self, tile)
            local up = self:getTile(tile.x, tile.y - 1)
            if up and up.info.isFloor then
                return true
            end
            local down = self:getTile(tile.x, tile.y + 1)
            if down and down.info.isFloor then
                return true
            end
            local left = self:getTile(tile.x - 1, tile.y)
            if left and left.info.isFloor then
                return true
            end
            local right = self:getTile(tile.x + 1, tile.y)
            if right and right.info.isFloor then
                return true
            end
            return false
        end,

        --- Checks if the given tile (assumed to be within this room) is near the edge of the room.
        --- @param self LibLevelGen.Room # The room.
        --- @param tile LibLevelGen.Tile # The tile to check.
        --- @return boolean
        isTileNearBorder = function(self, tile)
            return tile.x == 0 or tile.y == 0 or tile.x == self.w - 1 or tile.y == self.h - 1
        end,

        --- @class LibLevelGen.RoomFillParameters : LibLevelGen.TileData
        --- @field rect LibLevelGen.Rect | nil

        --- Fill the room.
        --- @param self LibLevelGen.Room # The room.
        --- @param parameters LibLevelGen.RoomFillParameters | string # Either the RoomFillParameters object or simply the tile type string.
        fill = function(self, parameters)
            if type(parameters) == "string" then
                parameters = {tileType = parameters}
            end
            local tileset = parameters.tileset
            local tileType = parameters.tileType
            local rect = parameters.rect or {x = 0, y = 0, w = self.w, h = self.h}
            for x = rect.x, rect.x + rect.w - 1 do
                for y = rect.y, rect.y + rect.h - 1 do
                    self:setTile(x, y, tileType, tileset)
                end
            end
        end,

        --- @class LibLevelGen.RoomBorderSideCount
        --- @field top number # Top tile count.
        --- @field bottom number # Bottom tile count.
        --- @field left number # Left tile count.
        --- @field right number # Right tile count.

        --- @class LibLevelGen.RoomBorderParameters : LibLevelGen.TileData
        --- Object used to specify room border details.
        --- @field rect LibLevelGen.Rect # The rect around which to place the border.
        --- @field sides LibLevelGen.RoomBorderSideCount # Definition of wall count on each side.

        --- Create a border around the room.
        --- @param self LibLevelGen.Room # The room.
        --- @param parameters LibLevelGen.RoomBorderParameters | string # Either a `RoomBorderParameters` specifying the border details, or simply a string tile type to create a basic border.
        border = function(self, parameters)
            if type(parameters) == "string" then
                parameters = {tileType = parameters}
            end
            local tileset = parameters.tileset
            local tileType = parameters.tileType
            local rect = parameters.rect or {x = 0, y = 0, w = self.w, h = self.h}
            local sides = parameters.sides or {top = 1, bottom = 1, left = 1, right = 1}
            if sides.top and sides.top > 0 then
                for x = rect.x, rect.x + rect.w - 1 do
                    for i = 0, sides.top - 1 do
                        self:setTile(x, rect.y + i, tileType, tileset)
                    end
                end
            end
            if sides.bottom and sides.bottom > 0 then
                for x = rect.x, rect.x + rect.w - 1 do
                    for i = 0, sides.bottom - 1 do
                        self:setTile(x, rect.y + rect.h - 1 - i, tileType, tileset) 
                    end
                end
            end
            if sides.left and sides.left > 0 then
                for y = rect.y, rect.y + rect.h - 1 do
                    for i = 0, sides.left - 1 do
                        self:setTile(rect.x + i, y, tileType, tileset) 
                    end
                end
            end
            if sides.right and sides.right > 0 then
                for y = rect.y, rect.y + rect.h - 1 do
                    for i = 0, sides.right - 1 do
                        self:setTile(rect.x + rect.w - 1 - i, y, tileType, tileset)
                    end
                end
            end
        end,

        --- Returns all tiles that meet the requirements.
        --- @param self LibLevelGen.Room # The room.
        --- @param requirements LibLevelGen.TileRequirements # Tile requirements to check against.
        --- @return LibLevelGen.Tile[]
        selectTiles = function(self, requirements)
            local res = {}
            for x = 0, self.w - 1 do
                for y = 0, self.h - 1 do
                    local tile = self:getTile(x, y)
                    if tile and tile:meetsRequirements(requirements) then
                        res[#res+1] = tile
                    end
                end
            end
            return res
        end,

        --- Choose a random tile out of ones that meet requirements
        --- @param self LibLevelGen.Room # The room.
        --- @param requirements LibLevelGen.TileRequirements # Tile requirements to check against.
        --- @return LibLevelGen.Tile
        chooseRandomTile = function(self, requirements)
            return self.instance:randChoice(self:selectTiles(requirements))
        end,

        --- Choose multiple tiles out of ones that meet requirements.
        --- @param self LibLevelGen.Room # The room.
        --- @param num number # Amount of tiles to choose.
        --- @param requirements LibLevelGen.TileRequirements # Tile requirements to check against.
        --- @return LibLevelGen.Tile[]
        chooseRandomTiles = function(self, num, requirements)
            return self.instance:randChoiceMany(self:selectTiles(requirements), num, true)
        end,

        --- Place a wall torch on the given tile.
        --- @param self LibLevelGen.Room # The room.
        --- @param tile LibLevelGen.Tile # Tile to place the torch on.
        --- @param type? string # Optional walltorch entity type override.
        placeWallTorch = function(self, tile, type)
            -- TODO: default torch type based on zone on the tile
            tile.hasWallTorch = true
            return self:placeEntity(tile, type or "WallTorch")
        end,

        --- Place given amount of wall torches randomly on elligible tiles.
        --- @param self LibLevelGen.Room # The room.
        --- @param num number # Amount of weall torches to place.
        --- @param type? string # Optional walltorch entity type override.
        placeWallTorches = function(self, num, type)
            -- TODO: prevent wall torches on adjacent tiles
            local elligibleTiles = self:selectTiles {
                isWall = true,
                noWallTorch = true,
                notCollision = collision.Type.FIXTURE
            }
            local torchTiles = self.instance:randChoiceMany(elligibleTiles, num, true)
            for i = 1, #torchTiles do
                self:placeWallTorch(torchTiles[i], type)
            end
        end,

        --- Place entity on the given tile (wrapper around tile:placeEntity that checks if tile is not nil)
        --- @param self LibLevelGen.Room # The room.
        --- @param tile LibLevelGen.Tile # The tile to place the entity on.
        --- @param entityOrType string | LibLevelGen.Entity # Existing entity or an entity type to spawn.
        --- @param level? number # Optional entity level if passing a string entity type.
        placeEntity = function(self, tile, entityOrType, level)
            if tile then
                return tile:placeEntity(entityOrType, level)
            end
        end,

        --- Place entity on a tile on given coordinates.
        --- @param self LibLevelGen.Room # The room.
        --- @param x number # X coordinate.
        --- @param y number # Y coordinate.
        --- @param entityOrType string | LibLevelGen.Entity # Existing entity or an entity type to spawn.
        --- @param level? number # Optional entity level if passing a string entity type.
        placeEntityAt = function(self, x, y, entityOrType, level)
            return self:placeEntity(self:getTile(x, y), entityOrType, level)
        end,

        --- Place entity on a random tile that meets requrements.
        --- @param self LibLevelGen.Room # The room.
        --- @param requirements LibLevelGen.TileRequirements # Requirements used to select the tile.
        --- @param entityOrType string | LibLevelGen.Entity # Existing entity or an entity type to spawn.
        --- @param level? number # Optional entity level if passing a string entity type.
        placeEntityRand = function(self, requirements, entityOrType, level)
            return self:placeEntity(self:chooseRandomTile(requirements), entityOrType, level)
        end,

        --- Place wire on the given tile.
        --- @param self LibLevelGen.Room # The room
        --- @param tile LibLevelGen.Tile # Tile to place the wire on.
        --- @param connectivity number # Unused, reserved.
        placeWire = function(self, tile, connectivity)
            if tile then
                tile.wire = connectivity
                tile:convert(tile.type == "DoorHorizontal" and "WireDoorHorizontal" or "Wire")
            end
        end,

        --- Turn the room into an exit room - set appropriate flags, place the exit stairs and the miniboss.
        --- @param self LibLevelGen.Room # The room.
        --- @param minibosses table # List of possible minibosses, in format: `{ {"type1", level1}, {"type2", level2}, ... }`
        makeExit = function(self, minibosses)
            self.flags = room.Type.EXIT
            self:placeExit()
            self:placeMiniboss(minibosses)
        end,

        --- Place exit somewhere in the room.
        --- @param self LibLevelGen.Room # The room.
        placeExit = function(self)
            local tile = self:chooseRandomTile(tr.Terrain.Exit)
            if not tile then
                error("No tiles are elligible for exit in the exit room")
            end
            tile:convert("LockedStairs")
            self.exit = {
                x = tile.x,
                y = tile.y
            }
        end,

        --- !!! Internal
        pickMiniboss = function(self, minibosses, minibossSeenFlags)
            local minimumSeenFlag = math.huge
            for _, seen in pairs(minibossSeenFlags) do
                if seen < minimumSeenFlag then
                    minimumSeenFlag = seen
                end
            end

            if minimumSeenFlag == math.huge then
                error("minimumSeenFlag is still math.huge (HOW) (empty miniboss list passed?)")
            end

            local elligibleMinibosses = {}
            for minibossType, seen in pairs(minibossSeenFlags) do
                if seen == minimumSeenFlag then
                    elligibleMinibosses[#elligibleMinibosses+1] = minibossType
                end
            end

            local pickedType = self.instance:randChoice(elligibleMinibosses)
            for i = 1, #minibosses do
                if minibosses[i][1] == pickedType then
                    minibossSeenFlags[pickedType] = minibossSeenFlags[pickedType] + 1
                    return minibosses[i]
                end
            end
            error("failed to pick miniboss (should never happen unless you literally passed a list of 0 possible minibosses)")
        end,
        -- minibosses: {{"type", level}, ...}

        --- Place a miniboss in the room (assumes that the room is an exit room).
        --- @param self LibLevelGen.Room # The room.
        --- @param minibosses table # In format `{{"type", level}, ...}`.
        placeMiniboss = function(self, minibosses)
            local previousLevelMinibosses = self.instance.runState.previousLevelMinibosses
            local thisLevelMinibosses = {}

            if not self.exit then
                error("placeMiniboss called on a room with no exit")
            end

            local minibossSeenFlags = {}

            for i = 1, #minibosses do
                local minibossType = minibosses[i][1]
                minibossSeenFlags[minibossType] = 0
            end

            for i = 1, #previousLevelMinibosses do
                local minibossType = previousLevelMinibosses[i]
                local val = minibossSeenFlags[minibossType]
                if val then
                    minibossSeenFlags[minibossType] = val + 1
                end
            end

            -- TODO: tempo/hard mode 2 minibosses by default
            local minibossCount = 1
            if self.instance.runState.bossShrineActive then
                minibossCount = minibossCount + 1
            end

            for i = 1, minibossCount do
                local minibossData = self:pickMiniboss(minibosses, minibossSeenFlags)
                thisLevelMinibosses[#thisLevelMinibosses+1] = minibossData[1]

                if i == 1 then
                    self:placeEntityAt(self.exit.x, self.exit.y, minibossData[1], minibossData[2])
                else
                    self:placeEntity(self:chooseRandomTile(levelGenUtil.TileRequirements.Enemy.Generic), minibossData[1], minibossData[2])
                end
            end

            if self.instance.runState.shopkeeperGhostLevel == self.instance:getFloor() and
                self.instance.runState.shopkeeperGhostDepth == self.instance:getDepth()
             then
                self:placeEntityAt(self.exit.x, self.exit.y, "ShopkeeperGhost")
            end

            self.instance.runState.previousLevelMinibosses = thisLevelMinibosses
        end,

        --- Randomly convert tiles from sourceType to targetType, chance of conversion happening is ratio.
        --- @param self LibLevelGen.Room # The room.
        --- @param sourceType string # Tile type to convert from.
        --- @param targetType string # Tile type to convert to.
        --- @param ratio number # Chance of the conversion happening, a float between 0.0 and 1.0.
        randomlyConvertTiles = function(self, sourceType, targetType, ratio)
            local elligibleTiles = self:selectTiles {
                tileType = sourceType
            }
            local tilesToConvert = self.instance:randChoiceMany(elligibleTiles, math.floor(#elligibleTiles * ratio), true)
            for i = 1, #tilesToConvert do
                tilesToConvert[i]:convert(targetType)
            end
        end,

        --- Convert all tiles from source type to target type, preserving their tileset.
        --- @param self LibLevelGen.Room # The room.
        --- @param sourceType string # Tile type to convert from.
        --- @param targetType string # Tile type to convert to.
        convertTiles = function(self, sourceType, targetType)
            local tilesToConvert = self:selectTiles {
                tileType = sourceType
            }
            for i = 1, #tilesToConvert do
                tilesToConvert[i]:convert(targetType)
            end
        end,

        --- Choose 1 tile that meets requirement and convert it and other nearby tiles to target type.
        --- @param self LibLevelGen.Room # The room.
        --- @param targetType string # Tiletype to convert the tiles to.
        --- @param requirements LibLevelGen.TileRequirements # The tile requirements used to pick the initial tile and the subsequent tiles.
        --- @param minConvert number # Minimum number of tiles to convert.
        --- @param maxConvert number # Maximum number of tiles to convert.
        --- @param initialTile? LibLevelGen.Tile # Can be used to specify initial tile, instead of choosing it randomly.
        spreadTile = function(self, targetType, requirements, minConvert, maxConvert, initialTile)
            local currentTile = initialTile or self:chooseRandomTile(requirements)
            local tilesToConvert = self.instance:randIntRange(minConvert, maxConvert + 1)
            --- @type LibLevelGen.Tile[]
            local convertedTiles = {}
            while tilesToConvert > 0 and currentTile do
                currentTile:convert(targetType)
                convertedTiles[#convertedTiles+1] = currentTile

                local nextTile
                while not nextTile do
                    --- @type LibLevelGen.Tile[]
                    local possibleNextTiles = {
                        self:getTile(currentTile.x + 1, currentTile.y),
                        self:getTile(currentTile.x - 1, currentTile.y),
                        self:getTile(currentTile.x, currentTile.y + 1),
                        self:getTile(currentTile.x, currentTile.y - 1),
                    }
                    util.removeIf(possibleNextTiles,
                        --- @param tile LibLevelGen.Tile
                        function(tile)
                            return not tile or not tile:meetsRequirements(requirements)
                        end
                    )
                    if #possibleNextTiles == 0 then
                        table.remove(convertedTiles, #convertedTiles)
                        if #convertedTiles == 0 then
                            log.warn("Room:spreadTile: failed to place all requested tiles")
                        end
                        currentTile = convertedTiles[#convertedTiles]
                    else
                        nextTile = self.instance:randChoice(possibleNextTiles)
                    end
                end
                currentTile = nextTile
                tilesToConvert = tilesToConvert - 1
            end
        end,

        --- Make the room a shop. TODO: move into a separate module
        --- @param self LibLevelGen.Room # The room
        --- @param locked? boolean # Whether the shop should be a locked shop
        --- @param force? boolean # Whether to force shopkeeper and items to spawn even if the shopkeeper killed flag is set in runState.
        makeShop = function(self, locked, force)
            -- TODO: monstrous shops
            -- TODO: prevent 2 items of the same slot
            local tileset = self.segment.tileset
            self.flags = room.Type.SHOP
            if locked == nil then
                locked = not self.instance.runState.lockedShopPlaced and self.instance:randChance(0.1)
                if locked then
                    self.segment.forceSecretRoomBomb = true
                    self.instance.runState.lockedShopPlaced = true
                end
            end
            self:fill {
                tileset = tileset,
                tileType = "Floor"
            }
            self:getTile(2, 3):convert("ShopFloor")
            self:getTile(4, 3):convert("ShopFloor")
            self:border {
                tileset = tileset,
                tileType = "ShopWall"
            }
            local firstLink = self.links[1]
            if firstLink then
                if locked then
                    self:getTile(firstLink.centerX, firstLink.centerY):convert("ShopWallCracked")
                else
                    self:getTile(firstLink.centerX, firstLink.centerY):convert(
                        self.instance:randChoice{"DoorHorizontal", "DoorHorizontal", "MetalDoorHorizontal"})
                end
            end
            self:placeWallTorches(11)

            -- TODO: logic to prevent multiple items of the same slot
            -- Items are rolled even if shopkeeper is dead to reduce variance
            local chestPosition = not locked and self.instance:randChance(0.25) and self.instance:randIntRange(1, 4) or -1
            local chestPool = chestPosition ~= -1 and
                self.instance:randChoice{itemGeneration.Pool.RED_CHEST, itemGeneration.Pool.PURPLE_CHEST, itemGeneration.Pool.BLACK_CHEST}

            local pool = locked and itemGeneration.Pool.LOCKED_SHOP or itemGeneration.Pool.SHOP

            --- @type ItemGeneration.ChoiceArguments[]
            local itemGenArgs = {
                {itemPool = pool},
                {itemPool = pool},
                {itemPool = pool}
            }
            if chestPool then
                itemGenArgs[chestPosition].requiredComponents = {chestPool}
            end

            local discount = locked and 0.5

            local itemEntities = {
                {
                    entityType = itemGeneration.choice(itemGenArgs[1]),
                    x = 2,
                    y = 5,
                },
                {
                    entityType = itemGeneration.choice(itemGenArgs[2]),
                    x = 3,
                    y = 5,
                },
                {
                    entityType = itemGeneration.choice(itemGenArgs[3]),
                    x = 4,
                    y = 5,
                },
            }

            if chestPosition ~= -1 then
                local it = itemEntities[chestPosition]
                it.chest = loot.chestForItem(it.entityType)
            end

            if not self.instance.runState.shopkeeperDead and not force then
                self:placeEntity(self:getTile(3, 3), "Shopkeeper")
                for _, it in ipairs(itemEntities) do
                    --- @type LibLevelGen.Entity
                    local saleEntity
                    if it.chest then
                        saleEntity = self:placeEntity(self:getTile(it.x, it.y), it.chest)
                        saleEntity:storeItem(it.entityType)
                    else
                        saleEntity = self:placeEntity(self:getTile(it.x, it.y), it.entityType)
                    end
                    saleEntity:setPrice()
                    if discount then
                        saleEntity:setDiscount(discount)
                    end
                end
            end
        end,
    }
}

--- Create a new room within the segment.
--- @param instance LibLegelGen.Instance # The instance.
--- @param segment LibLevelGen.Segment # The segment.
--- @param x number # X coordinate of room's top-left corner.
--- @param y number # Y coordinate of room's top-left corner.
--- @param w number # Room width.
--- @param h number # Room height.
--- @param flags number # Room flags.
--- @return LibLevelGen.Room
function room.new(instance, segment, x, y, w, h, flags)
    local newRoom = {}
    setmetatable(newRoom, mt)
    newRoom.instance = instance
    newRoom.segment = segment
    newRoom:init(x, y, w, h, flags)
    return newRoom
end

return room
