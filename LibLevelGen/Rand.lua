local util = require "system.utils.Utilities"

local rand = {}

local mt = {
    --- @class LibLevelGen.Rng
    --- Helper class for generating random numbers given a starting seed.
    __index = {
        seed = 1,
        currentSeed = 1,

        --- Generate a random 32-bit integer number.
        --- @param self LibLevelGen.Rng # The RNG instance
        --- @return number
        rand = function(self)
            local tmp = self.currentSeed ~= 0 and self.currentSeed or 1
            tmp = bit.bxor(tmp, bit.lshift(tmp, 13))
            tmp = bit.bxor(tmp, bit.rshift(tmp, 17))
            tmp = bit.bxor(tmp, bit.lshift(tmp, 5))
            self.currentSeed = tmp
            return tmp
        end,

        --- Generate a random float within the given range.
        --- @param self LibLevelGen.Rng # The RNG instance
        --- @param min number # Minimum value
        --- @param max number # Maximum value
        --- @return number
        randFloatRange = function (self, min, max)
            local rand = self:rand()
            return min + ((rand % 0x7fffffff) / 0x7fffffff) * (max - min)
        end,

        --- Generate a random integer within the given range.
        --- @param self LibLevelGen.Rng # The RNG instance
        --- @param min number # Minimum value (inclusive)
        --- @param max number # Maximum value (not inclusive)
        --- @return number
        randIntRange = function(self, min, max)
            local rand = self:rand()
            return (rand % (max - min)) + min
        end,

        --- Choose a random element from the given table.
        --- @generic T
        --- @param self LibLevelGen.Rng # The RNG instance
        --- @param t T[] # Elements to choose from
        --- @param remove? boolean # If true, element will be removed from the original table.
        --- @return T
        randChoice = function(self, t, remove)
            if #t == 0 then
                return nil
            end
            local index = self:randIntRange(1, #t + 1)
            local res = t[index]
            if remove then
                t[index] = t[#t]
                table.remove(t, #t)
            end
            return res
        end,

        --- Choose a n elements from the given table.
        --- @generic T
        --- @param self LibLevelGen.Rng # The RNG instance
        --- @param choices T[] # Elements to choose from
        --- @param n number # Number of elements to choose
        --- @param remove boolean # If true, element will be removed from the original table.
        --- @return T
        randChoiceMany = function(self, choices, n, remove)
            local t = not remove and util.shallowCopy(choices) or choices
            local res = {}

            for _ = 1, n do
                if #t == 0 then
                    return res
                end
                local index = self:randIntRange(1, #t + 1)
                res[#res+1] = t[index]
                t[index] = t[#t]
                table.remove(t, #t)
            end

            return res
        end,

        --- Randomly returns boolean based on given probability.
        --- @param self LibLevelGen.Rng # The RNG instance
        --- @param probability number # Value between 0.0 and 1.0
        --- @return boolean
        randChance = function(self, probability)
            local rand = self:rand()
            return (rand % 1000000) / 1000000 <= probability
        end,

        --- Randomly choose a value from the weighted choice data.
        --- @param self LibLevelGen.Rng # The RNG instance
        --- @param chances table<string,number> # Mapping of values that can be chosen to their probability (does not have to add up to 1)
        --- @return string
        randWeightedChoice = function(self, chances)
            local sum = 0
            local ranges = {}
            for value, chance in pairs(chances) do
                ranges[#ranges+1] = {value, sum + chance}
                sum = sum + chance
            end
            local rand = self:randFloatRange(0, sum)

            local i = 1
            while ranges[i][2] < rand do
                i = i + 1
            end

            return ranges[i][1]
        end,
    }
}

--- Create a new random number generator instance.
--- @param seed number # Initial seed
--- @return LibLevelGen.Rng
function rand.new(seed)
    local res = {}
    setmetatable(res, mt)
    res.currentSeed = seed
    return res
end

return rand
