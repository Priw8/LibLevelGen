local enum = require "system.utils.Enum"
local settings = require "necro.config.Settings"

local levelgenSettings = {}

levelgenSettings.Generator = enum.sequence {
    DEFAULT = 0,
    NECROLEVEL = 1,
}
retryOnFailmap = settings.user.bool {
    name = "Retry on failmap",
    default = true
}

defaultGenerator = levelgenSettings.Generator.NECROLEVEL

--- @param type number
function levelgenSettings.setDefaultGenerator(type)
    defaultGenerator = type
end

--- @return boolean
function levelgenSettings.retryOnFailmap()
    return retryOnFailmap
end

enableErrorLogging = settings.user.bool {
    id = "enableErrorLogging",
    name = "Enable error logging"
}

--- @return boolean
function levelgenSettings.enableErrorLogging()
    return enableErrorLogging
end

return levelgenSettings
