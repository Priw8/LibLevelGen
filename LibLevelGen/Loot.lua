local currency = require "necro.game.item.Currency"
local itemGeneration = require "necro.game.item.ItemGeneration"
local itemSlot = require "necro.game.item.ItemSlot"
local ecs = require "system.game.Entities"

local entity = require "LibLevelGen.Entity"
local room = require "LibLevelGen.Room"
local util = require "LibLevelGen.Util"

local tr = util.TileRequirements

local loot = {}

--- Get chest entity type for a given entity type.
--- @param itemType string # Item entity type.
--- @return string
function loot.chestForItem(itemType)
    local proto = ecs.getEntityPrototype(itemType)
    if proto.itemPoolRedChest then
        return "ChestRed"
    elseif proto.itemPoolBlackChest then
        return "ChestBlack"
    elseif proto.itemPoolPurpleChest then
        return "ChestPurple"
    end
    return "ChestRed" -- fallback
end

--- Creates a chest containing a given item type.
--- @param instance LibLegelGen.Instance # The LibLevelGen instance of generated level.
--- @param itemType string # Item type to put inside of the chest.
--- @param hidden boolean # Whether the chest is only revealed when next to it.
--- @param entityType? string # Optional chest entity type (defaults to chest color based on the item)
--- @param entityLevel? number # Optional chest entity level
--- @return LibLevelGen.Entity
function loot.createChestWithItem(instance, itemType, hidden, entityType, entityLevel)
    local chestEntityType = entityType or loot.chestForItem(itemType)
    local chestEntity = entity.new(instance, chestEntityType, -1, -1, entityLevel)
    chestEntity:storeItem(itemType)
    if hidden then
        chestEntity:setAttribute("hiddenUntilProximity", "hidden", true)
    end
    return chestEntity
end

--- @class LibLevelGen.ChestChoiceArguments : ItemGeneration.ChoiceArguments
--- @field hidden? boolean # Whether the chest starts visible or not
--- @field chestType? string # Chest entity type (defaults to chest color based on the item)
--- @field chestLevel? number # Chest entity type level
--- Interface for defining arguments of `loot.createChest`.

--- Creates a chest containing random item.
--- @param instance LibLegelGen.Instance # The LibLevelGen instance of the generated level.
--- @param args? LibLevelGen.ChestChoiceArguments # Extra parameters for the chest.
function loot.createChest(instance, args)
    if not args then
        args = {itemPool = itemGeneration.Pool.CHEST}
    end
    local itemType = itemGeneration.choice(args)
    return loot.createChestWithItem(instance, itemType, args.hidden, args.chestType, args.chestLevel)
end

--- Create a gold entity.
--- @param instance LibLegelGen.Instance # The LibLevelGen instance of the generated level.
--- @param amount number # gold amount to create
--- @return LibLevelGen.Entity
function loot.createGold(instance, amount)
    local entityType = currency.getItemForAmount(currency.Type.GOLD, amount)
    local goldEntity = entity.new(instance, entityType)
    goldEntity:setQuantity(amount)
    return goldEntity
end

--- Create an urn entity.
--- @param instance LibLegelGen.Instance # The LibLevelGen instance of the generated level.
--- @return LibLevelGen.Entity
function loot.createUrn(instance)
    local tehUrn = entity.new(instance, "Crate", -1, -1, 3)

    local item1 = itemGeneration.choice {itemPool = itemGeneration.Pool.URN}

    local item2
    local boost = 0
    while not item2 and boost < 5 do
        item2 = itemGeneration.choice {itemPool = itemGeneration.Pool.CHEST, slot = itemSlot.Type.SPELL, levelBonus = boost}
        boost = boost + 1
    end
    if not item2 then
        item2 = "ResourceHoardGold"
    end

    local item3 = itemGeneration.choice {itemPool = itemGeneration.Pool.FOOD, levelBonus = 2}

    tehUrn:storeItem(item1):storeItem(item2):storeItem(item3)

    return tehUrn
end

--- @param instance LibLegelGen.Instance
--- @param crateLevel number
function entityTypeForCrate(instance, crateLevel)
    local floor = instance:getFloor()
    if crateLevel == 1 then -- crate
        return "Skeleton" .. (floor > 1 and tostring(math.min(3, floor)) or "")
    else -- barrel
        return "Monkey"
    end
end


--- Create a create entity.
--- @param instance LibLegelGen.Instance # The LibLevelGen instance of the generated level.
--- @param crateTypeOverride? string # Force the crate to be a specific entity type.
--- @param crateLevelOverride? number # Force the crate to have a specific level.
--- @return LibLevelGen.Entity
function loot.createCrate(instance, crateTypeOverride, crateLevelOverride)
    -- 1 - crate, 2 - barrel
    local crateLevel = crateLevelOverride or instance:randIntRange(1, 3)

    --- @type LibLevelGen.Entity
    local crate

    if not crateTypeOverride and instance:randChance(0.03) then
        crate = entity.new(instance, "Trapchest", -1, -1, 3 + crateLevel)
    else
        crate = entity.new(instance, crateTypeOverride or "Crate", -1, -1, crateLevel)
        local itemRoll = instance:randIntRange(0, 101)
        if itemRoll <= 30 or (crateLevel ~= 2 and itemRoll <= 40) then
            itemRoll = instance:randIntRange(0, 101)
            if itemRoll <= 10 then
                crate:setAttribute("spawnOnDeath", "type", entityTypeForCrate(instance, crateLevel))
            elseif itemRoll <= 15 then
                crate:storeItem("Bomb3")
            elseif itemRoll <= 45 then
                crate:storeItem("Bomb")
            elseif itemRoll <= 76 then
                crate:storeItem("ResourceCoin10")
            elseif itemRoll <= 98 then
                crate:storeItem("ResourceCoin30")
            else
                crate:storeItem("ResourceCoin50")
            end
        else
            if instance:randChance(0.55) then
                crate:storeItem(itemGeneration.choice {itemPool = itemGeneration.Pool.LOCKED_CHEST})
            else
                local foodRoll = instance:randIntRange(0, 151)
                if foodRoll <= 40 then
                    crate:storeItem("Food1")
                elseif foodRoll <= 70 then
                    crate:storeItem("Food2")
                elseif foodRoll <= 90 then
                    crate:storeItem("Food3")
                elseif foodRoll <= 100 then
                    crate:storeItem("Food4")
                elseif foodRoll <= 130 then
                    crate:storeItem("FoodCarrot")
                else
                    crate:storeItem("FoodCookies")
                end
            end
        end
    end

    return crate
end

--- Place remaining chests.
--- @param segment LibLevelGen.Segment # Segment in which the remaining chests should be placed.
function loot.placeChests(segment)
    local numChests = segment.chestsLeft
    if numChests <= 0 then
        return
    end

    local rooms = segment:selectRooms(room.Flag.ALLOW_CHEST)
    local chestRooms = segment.instance:randChoiceMany(rooms, numChests)
    if #chestRooms < numChests then
        error("There are not enough rooms to place chests in")
    end

    for _, chestRoom in ipairs(chestRooms) do
        local chest = loot.createChest(segment.instance)
        local tile = chestRoom:chooseRandomTile(tr.Loot.Chest)
        if not tile then
            error("chestRoom does not have valid tiles to place the chest")
        end
        tile:placeEntity(chest)
    end
end

--- Place remaining crates.
--- @param segment LibLevelGen.Segment # Segment in which the remaining crates should be placed.
function loot.placeCrates(segment)
    local numCrates = segment.cratesLeft
    if numCrates <= 0 then
        return
    end

    local rooms = segment:selectRooms(room.Flag.ALLOW_CRATE)
    local crateRooms = segment.instance:randChoiceMany(rooms, numCrates)
    if #crateRooms < numCrates then
        error("There are not enough rooms to place chests in")
    end

    for _, crateRoom in ipairs(crateRooms) do
        local crate = loot.createCrate(segment.instance)
        local tile = crateRoom:chooseRandomTile(tr.Loot.Generic)
        if not tile then
            error("crateRoom does not have valid tiles to place the crate")
        end
        tile:placeEntity(crate)
    end
end

return loot
